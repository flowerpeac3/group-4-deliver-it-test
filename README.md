# Group4-DeliverIT

[Click Here](https://trello.com/b/SZRYEzRz/group-4-deliverit) for our Trello board

![N|Solid](https://gitlab.com/htsekin/group4-deliverit/-/raw/master/DeliverIT-WithAuth.PNG)

Link to Swagger Documentation - http://localhost:5000/swagger/index.html

## How to Run the Project

1. Run SQLEXPRESS

2. Run solution: "DeliverIT.sln"

3. Check connection string in DeliverIT.API => startup.cs
*   options.UseSqlServer(@"Server=.\SQLEXPRESS;Database=DeliverITDB;Integrated Security=True")

4. If folder "Migration" is empty add migration (if folder is not empty go to 4.3) from 
* 	Package Manager Console:
* 	4.1 before start typing check "Defaul project to be: DeliverIT.Database".
* 	4.2 type command in PM> "add-migration Initial" (PM> add-migration Initial).
* 	4.3 next command is to create Database "DeliverITDB" on your Database Server SQLEXPRESS
* 	  PM> "update-database" (PM> update-database). 

5. Select Start Project in Visual Studio -> Solution Explorer -> 
* 	right click on "DeliverIT.API" and choose "Set as Startup Project"



## Test Employees
* EmployeeId = 1
* FirstName = "Petar"
* LastName = "Ivanov"
* Roles = Admin, Employee
* Email = "pivanov@gmail.com"

## Test Customers With Parcels
* CustomerId = 1
* FirstName = "Gonzo"
* LastName = "ivanov"
* Email = "ggg@gmail.com"

* CustomerId = 5
* FirstName = "Matey"
* LastName = "Kaziiski"
* Email = "mkaziiski@trento.com"


## Shipments 

###### === Get All (Private)  ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments

###### === Get (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/1

###### === Post - Create (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments

###### === Put - Update (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments
* Input: (valid)
```json
{
    "Status": "On the way"
}
```
###### === Delete (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/4
* Output: 200 Ok

###### === Get Customer Parcels (Private) ===
* Access: Customer
* Authentication Header
* credentials=ggg@gmail.com
* End Point: api/shipments/getcustomerparcels

* Access: Employee
* Authentication Header
* credentials=pivanov@gmail.com
* End Point: api/shipments/getcustomerparcels?id=5

###### === Search By Status (Private) === 
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/searchstatus?value=on+the+way

###### === Remove Parcel From Shipment ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/removeparcelshipment
* Input:
```json
{  
    "Identificator": "6q2nyzj1kw7A021O",
    "parcelTrackNumber": "ASkL1QLDYtHl0io4"
}
```
###### === Add Parcel To Shipment (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/addparceltoshipment
* Input:
```json 
{  
    "Identificator": "6q2nyzj1kw7A021O",
    "parcelTrackNumber": "ASkL1QLDYtHl0io4"
}
```
###### === Get Customer Parcel Status ===
* Access: Customer
* Authentication Header
* credentials=ggg@gmail.com
* End Point: api/shipments/status?value=ASkL1QLDYtHl0io4
* Output:

* Access: Employee
* Authentication Header
* credentials=pivanov@gmail.com
* End Point: api/shipments/status?id=1&value=ASkL1QLDYtHl0io4

###### === Filter by warehouse or customer (Private) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/filter?warehouse=Kometa+3
* End Point: api/shipments/filter?customeremail=ggg@gmail.com

###### === Get Warehouse Shipments Statuses ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/shipments/warehouse?name=kometa+3
* End Point: api/shipments/warehouse?name=Trento+WareHouse



##  Warehouses  

###### === Get All Warehouses ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/warehouses/locations

######	=== Get Warehouse Locations ===
* Access: Public
* End Point: api/warehouses/locations/1

###### === Get ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/warehouses/1


###### === Post (Create) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/warehouses
* Input:
```json
{
    "name": "London Warehouse",
    "streetName": "63  Crown Street",
    "cityName": "London",
    "countryName": "Uk"
}
```
###### === Put (Update) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/warehouses/3
* Input:
```json
{
    "name": "Bristol Warehouse",
    "streetName": "91 Church Rd",
    "cityName": "Bristol",
    "countryName": "Uk"
}
```

###### === Delete ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/warehouses/3



##  Customer  

###### === Get Customers Count ===
* Access: Public
* End Point: api/customers/getcustomerscount

######	=== Get All Customers ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers

###### === Get Customer ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers/1

###### === Post (Create) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers
* Input:
```json
{
    "firstName": "Luc",
    "lastName": "Besson",
    "email": "lbesson@mail.fr",
    "streetName": "2 Rue Boutebrie",
    "cityName": "Paris",
    "countryName": "France"
}
```
###### === Put (Update) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers/6
* Input:
```json
{
    "cityName": "Lion",
}
```
* Access: Customer
* credentials=lbesson@mail.fr
* End Point: api/customers
* Input:
```json
{
    "cityName": "Nice",
}
```
###### === Delete ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers/6


###### === Search ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/customers/search?email=ggg@gmail.com
* End Point: api/customers/search?fname=Petar
* End Point: api/customers/search?lname=Stoqnov
* End Point: api/customers/search?fname=Gonzo&lname=ivanov&parcel=yes


##  Employee  

###### === Get All Employees ===
* Access: Employee
* Employee credentials=pivanov@gmail.com
* End Point: api/employees

###### === Get Employee ===
* Access: Employee
* Employee credentials=pivanov@gmail.com
* End Point: api/employees/1

###### === Post (Create) ===
* Access: Employee
* Employee credentials=pivanov@gmail.com
* End Point: api/employees
* Input:
```json
{
    "firstName": "Elon",
    "lastName": "Musk",
    "email": "emusk@spacex.com",
    "streetName": "166 Liberty St",
    "cityName": "San Francisko",
    "countryName": "USA"
}
```
###### === Put (Update) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/employees/6
* Input:
```json
{
    "cityName": "Fremont"
}
```
* Access: Customer
* credentials=emusk@spacex.com
* End Point: api/employees
* Input:
```json
{
    "cityName": "Reno"
}
```
###### === Delete ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/employees/3

##  Parcel  

###### === Get All Parcels ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels
* api/parcels

* 	=== Get Parcel ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels/1

###### === Post (Create) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels
* Input:
* Not valid: Shimpment is "On the way" or "Completed".
```json
{
    "weight": 923.42,
    "customerName": "Gonzo ivanov",
    "parcelTrackNumber": "wbTW9dhCjBWU6zAq",
    "shipmentIdentificator": "6q2nyzj1kw7A021O",
    "employeeName": "Petar Ivanov",
    "warehouseName": "Kometa 3",
    "categoryName": "Electronic"
}
```
* Valid
```json
{
    "weight": 923.42,
    "customerName": "Gonzo ivanov",
    "parcelTrackNumber": "wbTW9dhCjBWU6zAq",
    "shipmentIdentificator": "cFLvluOq751rdGCX",
    "employeeName": "Petar Ivanov",
    "warehouseName": "Kometa 3",
    "categoryName": "Electronic"
}
```
###### === Put (Update) ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels/7
* Input:
* Not valid: Shimpment is "On the way" or "Completed".
```json
{
    "weight": 32,
    "customerName": "Gonzo ivanov",
    "parcelTrackNumber": "wbTW9dhCjBWU6zAq",
    "shipmentIdentificator": "6q2nyzj1kw7A021O",
    "employeeName": "Petar Ivanov",
    "warehouseName": "Kometa 3",
    "categoryName": "Clothing"
}
```
* Input:
* Valid
```json
{
    "weight": 32,
    "customerName": "Gonzo ivanov",
    "parcelTrackNumber": "wbTW9dhCjBWU6zAq",
    "shipmentIdentificator": "6b8f25a6-7cac-40d5-a92b-119949605b93",
    "employeeName": "Petar Ivanov",
    "warehouseName": "Kometa 3",
    "categoryName": "Clothing"
}
```

###### === Delete ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels/7

###### === Filter ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels/filter?filter=customerfirstname&value=Gonzo
* End Point: api/parcels/filter?filter=customerlastname&value=Kaziiski
* End Point: api/parcels/filter?filter=warehousename&value=Kometa+3
* End Point: api/parcels/filter?filter=category&value=Clothing

###### === Get All Parcels by Stataus ===
* Access: Employee
* credentials=pivanov@gmail.com
* End Point: api/parcels/parcels?name=Gonzo&status=pastparcels
* End Point: api/parcels/parcels?name=Gonzo&status=ontheway

* Access: Customer
* credentials=ggg@gmail.com
* End Point: api/parcels/parcels?status=pastparcels
* End Point: api/parcels/parcels?status=ontheway



##  Addresses  

###### === Get All Addresses ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/addresses

###### === Get Parcel ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/parcels/1


##  Countries  
###### === Get All Countries ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/countries

###### === Get Country ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/countries/1

##  Cities  

###### === Get All Cities ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/cities

###### === Get City ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/cities/1

##  Statuses  

###### === Get All Statuses ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/statuses

###### === Get City ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/statuses/1

##  Categories  

###### === Get All Categories ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* End Point: api/categories

###### === Get Category ===
* Access: Employee Or Customer
* Employee credentials=pivanov@gmail.com
* Customer credentials=ggg@gmail.com
* 8End Point: api/categories/1
