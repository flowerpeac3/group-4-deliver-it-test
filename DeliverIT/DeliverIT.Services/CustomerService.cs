﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Messages;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly DeliverITDbContext context;

        public CustomerService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public CustomerDTO Create(CustomerDTO newCustomer)
        {
            this.ValidateCustomer(newCustomer);

            var tryEmail = this.context.Customers.Any(c => c.Email.ToLower().Equals(newCustomer.Email.ToLower()));
            if (tryEmail)
                throw new ArgumentException(Message.existingEmail);

            var city = GetCountryCity(newCustomer.CountryName, newCustomer.CityName);

            var customer = new Customer
            {
                FirstName = newCustomer.FirstName,
                LastName = newCustomer.LastName,
                Email = newCustomer.Email,
                AddressForDelivery = new Address
                {
                    StreetName = newCustomer.StreetName,
                    City = city,
                }
            };

            this.context.Customers.Add(customer);
            this.context.SaveChanges();

            return new CustomerDTO(customer);
        }

        public bool Delete(int id)
        {
            var customer = this.context.Customers
                .Include(c => c.AddressForDelivery)
                .FirstOrDefault(c => c.CustomerId == id);
            

            if (customer == null)
            {
                throw new ArgumentNullException(string.Format(Message.customerDoesNotExist, id));
            }

            var address = customer.AddressForDelivery;
            
            address.IsDeleted = true;
            customer.IsDeleted = true;


            this.context.SaveChanges();

            return true;
        }

        public CustomerDTO Get(int id)
        {
            var customerModel = this.context.Customers
                .Include(c => c.AddressForDelivery)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
                .FirstOrDefault(c => c.CustomerId == id && c.IsDeleted == false);

            if (customerModel != null)
            {
                return new CustomerDTO(customerModel);
            }

            return null;
        }

        public Customer GetCustomerByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentException(Message.failToAuthenticate);

                var customerModel = this.context.Customers
                .Include(c => c.AddressForDelivery)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(c => c.Roles)
                .FirstOrDefault(c => c.Email.ToLower().Equals(email.ToLower()) && c.IsDeleted == false);

            return customerModel;
        }

        public Customer GetCustomerByFirstName(string firstName)
        {
            if (string.IsNullOrEmpty(firstName))
                throw new ArgumentException(Message.notValidInput);

            var customerModel = this.context.Customers
            .Include(c => c.AddressForDelivery)
            .ThenInclude(a => a.City)
            .ThenInclude(c => c.Country)
            .FirstOrDefault(c => c.FirstName.ToLower().Equals(firstName.ToLower()) && c.IsDeleted == false);

            if (customerModel == null)
                throw new ArgumentException(string.Format(Message.customerDoesNotExist, firstName));

            return customerModel;
        }

        public Customer GetCustomerByFullName(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                throw new ArgumentException(Message.notValidInput);

            var customerModel = this.context.Customers
            .Include(c => c.AddressForDelivery)
            .ThenInclude(a => a.City)
            .ThenInclude(c => c.Country)
            .FirstOrDefault(c => c.FirstName.ToLower().Equals(firstName.ToLower()) 
                            && c.LastName.ToLower().Equals(lastName.ToLower()) 
                            && c.IsDeleted == false);

            if (customerModel == null)
                throw new ArgumentException(string.Format(Message.failToAuthenticateCustomer, (firstName + " " + lastName)));

            return customerModel;
        }

        public IEnumerable<CustomerDTO> GetAll()
        {
            List<CustomerDTO> customersDTO = new List<CustomerDTO>();

            var customers = this.context.Customers
                .Include(c => c.AddressForDelivery)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Where(c => c.IsDeleted == false);

            foreach (var item in customers)
            {
                customersDTO.Add(new CustomerDTO(item));
            }

            return customersDTO;
        }

        public IEnumerable<CustomerDTO> Search(string email, string fname, string lname, string searchByparcel)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(fname) 
                && string.IsNullOrEmpty(lname) && string.IsNullOrEmpty(searchByparcel))
                throw new ArgumentException(Message.notValidSearchParams);

            var customersAll = this.context.Customers
                .Include(c => c.Parcels)
                    .ThenInclude(p => p.Shipment)
                        .ThenInclude(s => s.Status)
                .Include(c => c.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(c => c.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(c => c.AddressForDelivery)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .ToList();

            if (!string.IsNullOrWhiteSpace(email) && email.Contains("@") && string.IsNullOrWhiteSpace(fname) 
                && string.IsNullOrWhiteSpace(fname) && string.IsNullOrWhiteSpace(searchByparcel))
            {
                customersAll = customersAll.Where(c => c.Email.ToLower().Contains(email.ToLower()) && c.IsDeleted == false).ToList();
            }

            if (!string.IsNullOrWhiteSpace(fname) && string.IsNullOrWhiteSpace(lname) 
                && string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(searchByparcel))
            {
                customersAll = customersAll.Where(c => c.FirstName.ToLower().Contains(fname.ToLower())
                                                    && c.IsDeleted == false).ToList();
            }

            if (!string.IsNullOrWhiteSpace(lname) && string.IsNullOrWhiteSpace(fname)
                && string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(searchByparcel))
            {
                customersAll = customersAll.Where(c => c.LastName.ToLower().Contains(lname.ToLower())
                                                    && c.IsDeleted == false).ToList();
            }


            if (!string.IsNullOrWhiteSpace(searchByparcel) && searchByparcel.ToLower() == "yes" && !string.IsNullOrWhiteSpace(fname) 
                && !string.IsNullOrWhiteSpace(lname) && string.IsNullOrWhiteSpace(email))
            {
                Customer customer = customersAll.
                    FirstOrDefault(c => c.FirstName.ToLower().Equals(fname.ToLower()) 
                                                                    && c.LastName.ToLower().Equals(lname.ToLower()));

                var parcelsTmp = customer.Parcels.Where(p => p.Shipment.ArrivalDate == null).ToList();
                customer.Parcels.Clear();

                foreach (var item in parcelsTmp)
                {
                    customer.Parcels.Add(item);
                }


                List<CustomerDTO> customers = new List<CustomerDTO>();

                customers.Add(new CustomerDTO(customer));

                return customers;
            }

            var customersResult = new List<CustomerDTO>();

            foreach (var item in customersAll)
            {
                customersResult.Add(new CustomerDTO(item));
            }

            return customersResult;
        }

        public CustomerDTO Update(int id, CustomerDTO updateCustomer)
        {
            if (updateCustomer == null)
                throw new ArgumentException(Message.notValidCustomerModel);

            if (!string.IsNullOrEmpty(updateCustomer.Email))
            {
                var tryEmail = this.context.Customers.Any(c => c.Email.ToLower().Equals(updateCustomer.Email.ToLower()));

                if (tryEmail)
                    throw new ArgumentException(Message.existingEmail);
            }
                
            var customer = this.context.Customers
            .Include(c => c.AddressForDelivery)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
            .FirstOrDefault(c => c.CustomerId == id && c.IsDeleted == false)
            ?? throw new ArgumentException(string.Format(Message.customerDoesNotExist, id));

            if (customer.CustomerId != id)
                new ArgumentException(Message.cantUpdate);

            if (customer.IsDeleted == true)
                throw new ArgumentException(string.Format(Message.customerDoesNotExist, id));


            var city = GetCountryCity(
                (updateCustomer.CountryName ?? customer.AddressForDelivery.City.Country.Name), 
                (updateCustomer.CityName ?? customer.AddressForDelivery.City.Name));

            customer.AddressForDelivery.StreetName = updateCustomer.StreetName ?? customer.AddressForDelivery.StreetName;
            customer.AddressForDelivery.City = city;
            customer.FirstName = updateCustomer.FirstName ?? customer.FirstName;
            customer.LastName = updateCustomer.LastName ?? customer.LastName;
            customer.Email = updateCustomer.Email ?? customer.Email;
                
            this.context.SaveChanges();
                
            return new CustomerDTO(customer);
            
        }

        public int CustomersCounter()
        {
            return this.context.Customers.Where(c => c.IsDeleted == false).Count();
        }

        private City GetCountryCity(string countryName, string cityName)
        {
            var country = this.context.Countries.FirstOrDefault(c => c.Name.ToLower().Equals(countryName.ToLower()))
                                ?? new Country { Name = countryName };

            var city = this.context.Cities
                .FirstOrDefault(c => c.Name.ToLower().Equals(cityName.ToLower())
                    && c.Country.Name.ToLower().Equals(countryName.ToLower()))
                ?? new City()
                {
                    Name = cityName,
                    Country = country,
                };

            return city;
        }

        private bool ValidateCustomer(CustomerDTO customer)
        {
            if (customer == null)
                throw new ArgumentException(Message.notValidCustomerModel);

            if (customer.FirstName == null || customer.FirstName.Length < 3)
                throw new ArgumentException(Message.notValidFirstName);

            if (customer.LastName == null || customer.LastName.Length < 3)
                throw new ArgumentException(Message.notValidLastName);

            if (customer.Email == null || customer.Email.Length < 3)
                throw new ArgumentException(Message.notValidEmail);

            if (customer.CityName == null || customer.CityName.Length < 3)
                throw new ArgumentException(Message.notValidCity);

            if (customer.CountryName == null)
                throw new ArgumentException(Message.notValidCountry);

            if (customer.StreetName == null || customer.StreetName.Length < 3)
                throw new ArgumentException(Message.notValidAddress);

            return true;
        }
    }
}
