﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly DeliverITDbContext context;
        public CategoryService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public Category Create(Category newCategory)
        {
            if (string.IsNullOrEmpty(newCategory.Name))
                throw new ArgumentException(Message.notValidInput);

            this.context.Categories.Add(newCategory);
            this.context.SaveChanges();

            return newCategory;
        }

        public bool Delete(int id)
        {
            var category = this.context.Categories
                .FirstOrDefault(c => c.CategoryId == id);

            var deleted = this.context.Categories.Remove(category);
            this.context.SaveChanges();

            if (deleted == null)
                return false;

            return true;
        }

        public Category Get(int id)
        {
            var category = this.context.Categories.FirstOrDefault(c => c.CategoryId == id);

            if (category == null)
            {
                throw new ArgumentNullException();
            }

            return category;
        }

        public Category GetByName(string name)
        {
            var category = this.context.Categories
                .FirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower()));

            if (category == null)
            {
                throw new ArgumentNullException();
            }

            return category;
        }

        public IEnumerable<Category> GetAll()
        {
            return this.context.Categories;
        }

        public Category Update(int id, string newName)
        {

            var category = this.context.Categories.FirstOrDefault(c => c.CategoryId == id);

            if (category == null)
            {
                throw new ArgumentNullException();
            }

            category.Name = newName;
            this.context.SaveChanges();

            return category;
        }

    }
}
