﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class EmployeeService : IEmployeeService
    {
        private DeliverITDbContext context;
        public EmployeeService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public EmployeeDTO Create(Employee employeeEditor, EmployeeDTO newEmployee)
        {
            if(this.GetEmployeeByEmail(employeeEditor.Email).Roles.FirstOrDefault(r => r.Role.Name.ToLower().Equals("admin")) == null)
                throw new ArgumentException(Message.cantCreate);


            this.ValidateCustomer(newEmployee);

            var tryEmail = this.context.Employees.Any(c => c.Email.ToLower().Equals(newEmployee.Email.ToLower()));
            if (tryEmail)
                throw new ArgumentException(Message.existingEmail);

            var city = GetCountryCity(newEmployee.CountryName, newEmployee.CityName);

            var employee = new Employee
            {
                FirstName = newEmployee.FirstName,
                LastName = newEmployee.LastName,
                Email = newEmployee.Email,
                Address = new Address
                {
                    StreetName = newEmployee.StreetName,
                    City = city,
                }
            };

            this.context.Employees.Add(employee);
            this.context.SaveChanges();

            return new EmployeeDTO(employee);
        }

        public bool Delete(int id)
        {
            var employee = this.context.Employees
                .Include(e => e.Address)
                .FirstOrDefault(c => c.EmployeeId == id);

            if (employee == null)
            {
                throw new ArgumentNullException(string.Format(Message.employeeDoesNotExist, id));
            }

            var address = employee.Address;

            address.IsDeleted = true;
            employee.IsDeleted = true;


            this.context.SaveChanges();

            return true;
        }

        public EmployeeDTO Get(int id)
        {
            var employee = this.context.Employees
                .Include(e => e.Address)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
                .FirstOrDefault(e => e.EmployeeId == id && e.IsDeleted == false);

            if (employee != null)
            {
                return new EmployeeDTO(employee);
            }

            return null;
        }
        public Employee GetEmployeeByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentException(Message.failToAuthenticate);

            var employeeModel = this.context.Employees
            .Include(c => c.Address)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
            .Include(e => e.Roles)
                .ThenInclude(er => er.Role)
            .FirstOrDefault(c => c.Email.ToLower().Equals(email.ToLower()) && c.IsDeleted == false);

            return employeeModel;
        }

        public Employee GetEmployeeByName(string firstName)
        {
            if (string.IsNullOrEmpty(firstName))
                throw new ArgumentException(Message.notValidInput);

            var customerModel = this.context.Employees
            .Include(e => e.Address)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
            .FirstOrDefault(e => e.FirstName.ToLower().Equals(firstName.ToLower()) && e.IsDeleted == false);

            if (customerModel == null)
                throw new ArgumentException(string.Format(Message.failToAuthenticateCustomer, firstName));

            return customerModel;
        }
        public IEnumerable<EmployeeDTO> GetAll()
        {
            List<EmployeeDTO> employeesDTO = new List<EmployeeDTO>();

            var employees = this.context.Employees
                .Include(e => e.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Where(e => e.IsDeleted == false);

            
            foreach (var item in employees)
            {
                employeesDTO.Add(new EmployeeDTO(item));
            }

            return employeesDTO;
        }


        public EmployeeDTO Update(int id, EmployeeDTO updateEmployee)
        {
            if (updateEmployee == null)
                throw new ArgumentException(Message.notValidModelEmployee);

            if (!string.IsNullOrEmpty(updateEmployee.Email))
            {
                var tryEmail = this.context.Employees.Any(c => c.Email.ToLower().Equals(updateEmployee.Email.ToLower()));
                
                if (tryEmail)
                    throw new ArgumentException(Message.existingEmail);
            }

            var employee = this.context.Employees
            .Include(e => e.Address)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
            .FirstOrDefault(e => e.EmployeeId == id && e.IsDeleted == false)
            ?? throw new ArgumentException(string.Format(Message.employeeDoesNotExist, id));

            if (employee.EmployeeId != id)
                new ArgumentException(Message.cantUpdate);

            if (employee.IsDeleted == true)
                throw new ArgumentException(string.Format(Message.employeeDoesNotExist, id));

            var city = GetCountryCity(
                (updateEmployee.CountryName ?? employee.Address.City.Country.Name), 
                (updateEmployee.CityName ?? employee.Address.City.Name));

            employee.Address.StreetName = updateEmployee.StreetName ?? employee.Address.StreetName;
            employee.Address.City = city;
            employee.FirstName = updateEmployee.FirstName ?? employee.FirstName;
            employee.LastName = updateEmployee.LastName ?? employee.LastName;
            employee.Email = updateEmployee.Email ?? employee.Email;

            this.context.SaveChanges();

            return new EmployeeDTO(employee);

        }

        private City GetCountryCity(string countryName, string cityName)
        {
            var country = this.context.Countries.FirstOrDefault(e => e.Name.ToLower().Equals(countryName.ToLower()))
                                ?? new Country { Name = countryName };

            var city = this.context.Cities
                .FirstOrDefault(e => e.Name.ToLower().Equals(cityName.ToLower())
                    && e.Country.Name.ToLower().Equals(countryName.ToLower()))
                ?? new City()
                {
                    Name = cityName,
                    Country = country,
                };

            return city;
        }

        private bool ValidateCustomer(EmployeeDTO employee)
        {
            if (employee == null)
                throw new ArgumentException(Message.notValidModelEmployee);

            if (employee.FirstName == null || employee.FirstName.Length < 3)
                throw new ArgumentException(Message.notValidFirstName);

            if (employee.LastName == null || employee.LastName.Length < 3)
                throw new ArgumentException(Message.notValidLastName);

            if (employee.Email == null || employee.Email.Length < 3)
                throw new ArgumentException(Message.notValidEmail);

            if (employee.CityName == null || employee.CityName.Length < 3)
                throw new ArgumentException(Message.notValidCity);

            if (employee.CountryName == null)
                throw new ArgumentException(Message.notValidCountry);

            if (employee.StreetName == null || employee.StreetName.Length < 3)
                throw new ArgumentException(Message.notValidAddress);

            return true;
        }
    }
}
