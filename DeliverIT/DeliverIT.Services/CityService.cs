﻿using DeliverIT.Database;
using DeliverIT.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class CityService : ICityService
    {
        private readonly DeliverITDbContext context;
        public CityService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public City Create(City newCity)
        {

            this.context.Cities.Add(newCity);
            this.context.SaveChanges();

            return newCity;
        }

        public bool Delete(int id)
        {
            var city = this.context.Cities
                .FirstOrDefault(c => c.CityId == id);

            var deleted = this.context.Cities.Remove(city);
            this.context.SaveChanges();

            if (deleted == null)
                return false;

            return true;
        }

        public City Get(int id)
        {
            var city = this.context.Cities
                .Include(c => c.Country)
                .FirstOrDefault(c => c.CityId == id);

            if (city == null)
            {
                throw new ArgumentNullException();
            }

            return city;
        }

        public IEnumerable<City> GetAll()
        {
            return this.context.Cities.Include(c => c.Country);
        }

        public City Update(int id, string newName, int newCity)
        {

            var city = this.context.Cities.FirstOrDefault(c => c.CityId == id);

            if (city == null)
            {
                throw new ArgumentNullException();
            }

            city.Name = newName;
            city.CountryId = newCity;
            
            this.context.SaveChanges();

            return city;
        }
    }
}
