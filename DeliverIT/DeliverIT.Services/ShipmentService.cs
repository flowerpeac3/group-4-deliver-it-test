﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Messages;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services
{
    public class ShipmentService : IShipmentService
    {
        private readonly DeliverITDbContext context;
        private readonly IStatusService statusService;

        public ShipmentService(DeliverITDbContext context, IStatusService statusService)
        {
            this.context = context;
            this.statusService = statusService; //new StatusService(context);
        }

        public ShipmentDTO Create()
        {
            var initialStatus = this.statusService.GetAll().First();

            if (initialStatus == null)
                throw new ArgumentException(Message.messageErrorCreation);

            var shipment = new Shipment();
            shipment.Identificator = Guid.NewGuid().ToString();
            shipment.StatusId = initialStatus.StatusId;
            shipment.Status = initialStatus;

            
            this.context.Shipments.Add(shipment);
            this.context.SaveChanges();
            

            return new ShipmentDTO(shipment, this.statusService.Get(shipment.StatusId).Name);
        }


        public bool Delete(int id)
        {
            var shipment = this.context.Shipments
                .FirstOrDefault(s => s.ShipmentId == id && s.IsDeleted == false);

            if (shipment == null)
            {
                throw new ArgumentException(string.Format(Message.messageNotExist, id));
            }

            
            shipment.IsDeleted = true;
            this.context.SaveChanges();
           

            return true;
        }

        public ShipmentDTO Get(int id)
        {
            var shipment = this.context.Shipments
               .Include(s => s.Status)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .FirstOrDefault(e => e.ShipmentId == id && e.IsDeleted == false);

            if (shipment == null)
            {
                throw new ArgumentException(string.Format(Message.messageNotExist, id));
            }

            return new ShipmentDTO(shipment);
        }

        public Shipment GetShipmentByIdentificator(string identificator)
        {
            var shipment = this.context.Shipments
                .Include(s => s.Status)
                .Include(s => s.Parcels)
                .FirstOrDefault(e => e.Identificator.ToLower().Equals(identificator.ToLower()) && e.IsDeleted == false);

            if (shipment == null)
            {
                throw new ArgumentException(string.Format(Message.messageNotExist, identificator));
            }

            return shipment;
        }

        public IEnumerable<ShipmentDTO> GetAll()
        {
            List<ShipmentDTO> shipmentsDTO = new List<ShipmentDTO>();

            var shipments = this.context.Shipments
                .Include(s => s.Status)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Where(s => s.IsDeleted == false)
                .ToList();

            if (shipments == null)
            {
                throw new ArgumentException(Message.messageNoContent);
            }

            foreach (var item in shipments)
            {
                shipmentsDTO.Add(new ShipmentDTO(item));
            }

            return shipmentsDTO;

        }

        public IEnumerable<ShipmentDTO> GetByStatus(string status)
        {
            if (string.IsNullOrEmpty(status))
                throw new ArgumentException(Message.inputParamsNotValid);

            this.statusService.GetStatusByName(status.ToLower());
                
            var shipments = this.context.Shipments
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(s => s.Status)
                .Where(s => s.Status.Name.ToLower().Equals(status.ToLower()) && s.IsDeleted == false);

            List<ShipmentDTO> shipmentsDTO = new List<ShipmentDTO>();

            foreach (var item in shipments)
            {
                shipmentsDTO.Add(new ShipmentDTO(item));
            }

            return shipmentsDTO;
        }

        public ShipmentDTO AddParcelToShipment(ShipmentParcelDTO shipmentParcel)
        {
            if (string.IsNullOrEmpty(shipmentParcel.ParcelTrackNumber) && string.IsNullOrEmpty(shipmentParcel.Identificator))
                throw new ArgumentException(Message.inputParamsNotValid);

            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .FirstOrDefault(p => p.ParcelTrackNumber == shipmentParcel.ParcelTrackNumber)
                ?? throw new ArgumentException(string.Format(Message.notExistParsel, shipmentParcel.ParcelTrackNumber));

            var shipment = this.context.Shipments
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(s => s.Status)
                .FirstOrDefault(s => s.Identificator == shipmentParcel.Identificator)
                ?? throw new ArgumentException(string.Format(Message.notExistShipment, shipmentParcel.Identificator));


            if (shipment.Parcels.FirstOrDefault(p => p.ParcelId == parcel.ParcelId) != null)
                throw new ArgumentException(string.Format(Message.parcelIsAlreadyInShipment, shipmentParcel.ParcelTrackNumber, shipmentParcel.Identificator));

            shipment.Parcels.Add(parcel);
            this.context.SaveChanges();

            return new ShipmentDTO(shipment);
        }

        public ShipmentDTO RemoveParcelToShipment(ShipmentParcelDTO shipmentParcel)
        {
            if (string.IsNullOrEmpty(shipmentParcel.ParcelTrackNumber) || string.IsNullOrEmpty(shipmentParcel.Identificator))
                throw new ArgumentException(Message.inputParamsNotValid);

            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .FirstOrDefault(p => p.ParcelTrackNumber == shipmentParcel.ParcelTrackNumber)
                ?? throw new ArgumentException(string.Format(Message.notExistParsel, shipmentParcel.ParcelTrackNumber));

            var shipment = this.context.Shipments
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(s => s.Status)
                .FirstOrDefault(s => s.Identificator == shipmentParcel.Identificator)
                ?? throw new ArgumentException(string.Format(Message.notExistShipment, shipmentParcel.Identificator));

            if (shipment.Parcels.FirstOrDefault(s => s.ParcelId == parcel.ParcelId) == null)
                throw new ArgumentException(string.Format(Message.parcelIsNotInShipment, shipmentParcel.ParcelTrackNumber, shipmentParcel.Identificator));

            shipment.Parcels.Remove(parcel);
            this.context.SaveChanges();

            return new ShipmentDTO(shipment);
        }

        public List<ShipmentDTO> GetCustomerParcels(int id)
        {
            var shipment = this.context.Shipments
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(s => s.Status);

            var shipmentFilterted = from s in shipment
                                    where (s.Parcels.Where(p => p.CustomerId == id).Count()) > 0
                                    select new ShipmentDTO
                                    {
                                        Identificator = s.Identificator,
                                        DepartureDate = s.DepartureDate,
                                        ArrivalDate = s.ArrivalDate,
                                        Status = s.Status.Name,
                                        Parcels = (from p in s.Parcels
                                                   where p.CustomerId == id && p.ShipmentId == s.ShipmentId
                                                   select new ParcelDescriptionDTO
                                                   {
                                                       CustomerName = (p.Customer.FirstName + " " + p.Customer.LastName),
                                                       ParcelTrackNumber = p.ParcelTrackNumber,
                                                       Weight = p.Weight
                                                   }).ToList()
                                    };

            return shipmentFilterted.ToList();
        }


        public ParcelShipmentStatuslDTO GetCustomerParcelStatus(int id, string parcelTrackingNumber)
        {
            if (string.IsNullOrEmpty(parcelTrackingNumber))
                throw new ArgumentException(Message.inputParamsNotValid);

            // ToDo: remove after authentication has bee added
            var customer = this.context.Customers.FirstOrDefault(c => c.CustomerId == id);

            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Shipment)
                .ThenInclude(s => s.Status)
                .FirstOrDefault(p => p.ParcelTrackNumber == parcelTrackingNumber && p.Customer.Email.ToLower().Equals(customer.Email.ToLower()));

            if (parcel == null)
                throw new ArgumentException(Message.lackOfPermisionsParcelNotExist);

            return new ParcelShipmentStatuslDTO(parcelTrackingNumber, parcel.Shipment.Status.Name);
        }

        public ShipmentWarehouseDTO GetWarehouseShipmentsStatuses(string warehouseName)
        {
            if (string.IsNullOrEmpty(warehouseName))
                throw new ArgumentException(Message.inputParamsNotValid);

            var shipments = this.context.Shipments
                .Include(s => s.Status)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Where(s => s.Parcels.Any(p => p.Warehouse.Name.ToLower().Equals(warehouseName.ToLower())))
                ?? throw new ArgumentException(string.Format(Message.notExistWarehouse, warehouseName));


            var shipmentsAsString = new List<string>();

            foreach (var item in shipments)
            {
                shipmentsAsString.Add("Warehouse: " + item.Identificator + ", Status: " + item.Status.Name);
            }

            ShipmentWarehouseDTO incomingShipments = new ShipmentWarehouseDTO(warehouseName, shipmentsAsString);

            return incomingShipments;
        }

        public ShipmentDTO Update(int id, ShipmentDTO updateShipment)
        {
            var shipment = this.context.Shipments
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Employee)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Warehouse)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(s => s.Status)
                .FirstOrDefault(s => s.ShipmentId == id) ??
                   throw new ArgumentException(string.Format(Message.messageNotExist, id));

            if (shipment.Status.Name.ToLower() == updateShipment.Status.ToLower())
            {
                throw new ArgumentException(string.Format(Message.messageSameStatus, shipment.Status.Name));
            }

            var initialStatus = this.statusService
                .GetAll()
                .FirstOrDefault();
            
            var finalStatus = this.statusService
                .GetAll()
                .OrderByDescending(s => s.StatusId)
                .FirstOrDefault();


            if (initialStatus == null)
                throw new ArgumentException(Message.messageStatusTableEmpty);

            var validatedInputStatus = this.statusService
                .GetAll()
                .FirstOrDefault(s => s.Name == updateShipment.Status) ??
                   throw new ArgumentException(Message.messageNotValidStatus);


            if (!shipment.Status.Equals(finalStatus.Name))
            {

                if (shipment.Parcels.Count == 0)
                    throw new ArgumentException(Message.messageEmptyShipmentError);

                if (validatedInputStatus.Name.ToLower().Equals("on the way"))
                {
                    shipment.DepartureDate = DateTime.Now;
                    //updateShipment.DepartureDate = shipment.DepartureDate;
                    shipment.Status = validatedInputStatus;
                }

                if (validatedInputStatus.Name.ToLower().Equals("completed"))
                {
                    shipment.ArrivalDate = DateTime.Now;
                    //updateShipment.ArrivalDate = shipment.ArrivalDate;
                    shipment.Status = validatedInputStatus;
                }

            }
            else
            {
                throw new ArgumentException(Message.messageStatusAlreadyCompleted);
            }

            this.context.Shipments.Update(shipment);
            this.context.SaveChanges();

            return new ShipmentDTO(shipment);
        }

        public IEnumerable<ShipmentDTO> Filter(string warehouse, string email)
        {
            if (!string.IsNullOrEmpty(warehouse) && !string.IsNullOrEmpty(email))
                throw new ArgumentException(Message.messageFindWithTwoParams);

            if (!string.IsNullOrEmpty(warehouse))
            {

                var resultWarehouses = from s in this.context.Shipments
                                       join st in this.context.Statuses on s.StatusId equals st.StatusId
                                       join p in this.context.Parcels on s.ShipmentId equals p.ShipmentId
                                       join w in this.context.Warehouses on p.WarehouseId equals w.WarehouseId
                                       where w.Name.ToLower().Contains(warehouse.ToLower())
                                       select new ShipmentDTO(new Shipment
                                       {
                                           ShipmentId = s.ShipmentId,
                                           Identificator = s.Identificator,
                                           DepartureDate = s.DepartureDate,
                                           ArrivalDate = s.ArrivalDate,
                                           IsDeleted = s.IsDeleted,
                                           Status = st,
                                       });

                return resultWarehouses.ToList();
            }

            if (!string.IsNullOrEmpty(email))
            {
                var resultCustomers = from s in this.context.Shipments
                                      join st in this.context.Statuses on s.StatusId equals st.StatusId
                                      join p in this.context.Parcels on s.ShipmentId equals p.ShipmentId
                                      join c in this.context.Customers on p.CustomerId equals c.CustomerId
                                      where c.Email.ToLower().Equals(email.ToLower())
                                      select new ShipmentDTO(new Shipment
                                      {
                                          ShipmentId = s.ShipmentId,
                                          Identificator = s.Identificator,
                                          DepartureDate = s.DepartureDate,
                                          ArrivalDate = s.ArrivalDate,
                                          IsDeleted = s.IsDeleted,
                                          Status = st
                                      });

                return resultCustomers.ToList();
            }

            throw new ArgumentException(Message.messageFindWithoutParam);
        }
    }
}
