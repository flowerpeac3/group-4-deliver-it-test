﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services
{
    public class ParcelService : IParcelService
    {
        private readonly DeliverITDbContext context;
        private readonly IShipmentService shipmentService;
        private readonly ICustomerService customerService;
        private readonly IEmployeeService employeeService;
        private readonly IWarehouseService warehouseService;
        private readonly ICategoryService categoryService;

        public ParcelService(DeliverITDbContext context)
        {
            this.context = context;
        }
        public ParcelService(DeliverITDbContext context, IShipmentService shipmentService, ICustomerService customerService,
            IEmployeeService employeeService, IWarehouseService warehouseService, ICategoryService categoryService)
        {
            this.context = context;
            this.shipmentService = shipmentService; //new ShipmentService(context);
            this.customerService = customerService; //new CustomerService(context);
            this.employeeService = employeeService; // new EmployeeService(context);
            this.warehouseService = warehouseService; //new WarehouseService(context);
            this.categoryService = categoryService; //new CategoryService(context);
        }

        public ParcelDTO Create(ParcelDTO newParcel)
        {
            string[] customerNames = newParcel.CustomerName.Split();

            var checkCustomer = this.customerService.GetCustomerByFirstName(customerNames[0]);

            if (checkCustomer == null)
            {
                throw new ArgumentNullException();
            }

            var shipment = this.shipmentService.GetShipmentByIdentificator(newParcel.ShipmentIdentificator);

            if (!shipment.Status.Name.ToLower().Equals("preparing"))
                throw new ArgumentException(Message.shipmentIsNotPreparing);

            string[] employeeNames = newParcel.EmployeeName.Split();

            var checkEmployee = this.employeeService.GetEmployeeByName(employeeNames[0]);

            if (checkEmployee == null)
            {
                throw new ArgumentNullException();
            }

            var checkWarehouse = this.warehouseService.GetByName(newParcel.WarehouseName);

            if (checkWarehouse == null)
            {
                throw new ArgumentNullException();
            }

            var checkCategory = this.categoryService.GetByName(newParcel.CategoryName);

            if (checkCategory == null)
            {
                throw new ArgumentNullException();
            }

            if (newParcel.Weight < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            var parcel = new Parcel()
            {
                Weight = newParcel.Weight,
                Customer = checkCustomer,
                Shipment = shipment,
                ParcelTrackNumber = Guid.NewGuid().ToString(),
                Employee = checkEmployee,
                Warehouse = checkWarehouse,
                Category = checkCategory
            };

            this.context.Parcels.Add(parcel);
            this.context.SaveChanges();

            return new ParcelDTO(parcel);
        }

        public bool Delete(int id)
        {
            var parcel = this.context.Parcels
                .FirstOrDefault(p => p.ParcelId == id)
                ?? throw new ArgumentNullException("Cannot find parcel.");

            parcel.IsDeleted = true;

            this.context.SaveChanges();

            return true;
        }


        public ParcelDTO Get(int id)
        {
            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .FirstOrDefault(p => p.ParcelId == id && p.IsDeleted == false);

            if (parcel != null)
            {
                return new ParcelDTO(parcel);
            }

            return null;
        }

        public Parcel Get(string trackNumber)
        {
            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .FirstOrDefault(p => p.ParcelTrackNumber.ToLower().Equals(trackNumber.ToLower()) && p.IsDeleted == false);

            if (parcel != null)
            {
                return parcel;
            }

            return null;
        }

        public IEnumerable<ParcelDTO> GetAll()
        {
            List<ParcelDTO> parcelsDTO = new List<ParcelDTO>();

            var parcels = this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.IsDeleted == false);

            if (parcels != null)
            {
                foreach (var parcel in parcels)
                {
                    parcelsDTO.Add(new ParcelDTO(parcel));
                }

                return parcelsDTO;
            }

            return null;
        }

        public ParcelDTO Update(int id, ParcelDTO newParcel)
        {
            var parcel = this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .FirstOrDefault(p => p.ParcelId == id);

            if (!string.IsNullOrEmpty(newParcel.ShipmentIdentificator))
            {
                var shipment = this.shipmentService.GetShipmentByIdentificator(newParcel.ShipmentIdentificator);

                if (shipment != null)
                {
                    if (!shipment.Status.Name.ToLower().Equals("preparing"))
                        throw new ArgumentException(Message.shipmentIsNotPreparing);

                    parcel.Shipment = shipment;
                }
            }

            if (!string.IsNullOrEmpty(newParcel.CustomerName))
            {
                var customer = this.customerService.GetCustomerByFullName(newParcel.CustomerName.Split()[0], newParcel.CustomerName.Split()[1])
                    ?? throw new ArgumentException("Cannot change customer because it does not exist.");

                parcel.Customer = customer;
            }

            if (!string.IsNullOrEmpty(newParcel.WarehouseName))
            {
                var warehouse = this.warehouseService.GetByName(newParcel.WarehouseName)
                    ?? throw new ArgumentException($"Warehouse with name {newParcel.WarehouseName} does not exist.");

                parcel.Warehouse = warehouse;
            }

            if (!string.IsNullOrEmpty(newParcel.CategoryName))
            {
                var category = this.categoryService.GetByName(newParcel.CategoryName)
                    ?? throw new ArgumentException($"Category with name {newParcel.CategoryName} does not exist.");

                parcel.Category = category;
            }

            if (newParcel.Weight < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (newParcel.Weight >= 0)
            {
                parcel.Weight = newParcel.Weight;
            }

            this.context.SaveChanges();

            return new ParcelDTO(parcel);
        }
        public IEnumerable<ParcelDTO> FIlterByCustomerFirstName(string name)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Customer.FirstName == name))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }
        public IEnumerable<ParcelDTO> FIlterByCustomerLastName(string name)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Customer.LastName == name))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }
        public IEnumerable<ParcelDTO> FIlterByWarehouse(string name)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Warehouse.Name == name))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }
        public IEnumerable<ParcelDTO> FIlterByCategory(string name)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Category.Name == name))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }
        public IEnumerable<ParcelDTO> FIlterByWeight(double weight)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Weight == weight))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }

        public IEnumerable<ParcelDTO> PastParcels(string customerName)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Customer.FirstName == customerName)
                .Where(p => p.Shipment.Status.Name == "completed"))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }

        public IEnumerable<ParcelDTO> ParcelsOnTheWay(string customerName)
        {
            List<ParcelDTO> parcels = new List<ParcelDTO>();

            foreach (var parcel in this.context.Parcels
                .Include(p => p.Customer)
                .Include(p => p.Category)
                .Include(p => p.Employee)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .Include(p => p.Warehouse)
                .Where(p => p.Customer.FirstName == customerName)
                .Where(p => p.Shipment.Status.Name == "on the way"))
            {
                var dto = new ParcelDTO(parcel);
                parcels.Add(dto);
            }

            return parcels;
        }

        //public IEnumerable<ParcelDTO> ShowAllParcels(string customerName)
        //{
        //    List<ParcelDTO> parcels = new List<ParcelDTO>();

        //    foreach (var parcel in this.context.Parcels
        //        .Include(p => p.Customer)
        //        .Include(p => p.Category)
        //        .Include(p => p.Employee)
        //        .Include(p => p.Shipment)
        //            .ThenInclude(s => s.Status)
        //        .Include(p => p.Warehouse)
        //        .Where(p => p.Customer.)

        //    {
        //        var dto = new ParcelDTO(parcel);
        //        parcels.Add(dto);
        //    }

        //    return parcels;
        //}
    }
}
