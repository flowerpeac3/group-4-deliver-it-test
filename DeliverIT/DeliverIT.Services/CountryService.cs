﻿using DeliverIT.Database;
using DeliverIT.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class CountryService : ICountryService
    {
        private readonly DeliverITDbContext context;
        public CountryService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public Country Create(Country newCountry)
        {

            this.context.Countries.Add(newCountry);
            this.context.SaveChanges();

            return newCountry;
        }

        public bool Delete(int id)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.CountryId == id);

            try
            {
                var deleted = this.context.Countries.Remove(country);
                this.context.SaveChanges();

                if (deleted == null)
                    return false;

                return true;

            }
            catch (DbUpdateException)
            {
                return false;
            }
        }

        public Country Get(int id)
        {
            var country = this.context.Countries.FirstOrDefault(c => c.CountryId == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            return country;
        }

        public IEnumerable<Country> GetAll()
        {
            return this.context.Countries;
        }

        public Country Update(int id, string newName)
        {

            var country = this.context.Countries.FirstOrDefault(c => c.CountryId == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            country.Name = newName;
            
            this.context.SaveChanges();

            return country;
        }
    }
}
