﻿namespace DeliverIT.Services.Messages
{
    internal static class Message
    {
        // CustomerService also used in EmployeeService and WarehouseService
        public static readonly string notValidCustomerModel = "Need to fill all information to create a Customer!";
        public static readonly string notValidFirstName = "First Name that try to create is not valid!";
        public static readonly string notValidLastName = "Last Name that try to create is not valid!";
        public static readonly string notValidEmail = "Email that try to create is not valid!";
        public static readonly string notValidCountry = "Country that try to create is not valid!";
        public static readonly string notValidCity = "City that try to create is not valid!";
        public static readonly string notValidAddress = "Address that try to create is not valid!";
        public static readonly string customerDoesNotExist = "Customer with that Id: {0} does not exist!";
        public static readonly string notValidCountryOtCity = "Country or City that try to create is not valid!";
        public static readonly string existingEmail = "This email is already taken!";
        public static readonly string failToAuthenticateCustomer = "Customer with email: {0}, fail to authenticate!";
        public static readonly string notValidSearchParams = "You need to choose one params to search for! (byName, byName, byParcel).";

        //EmployeeService
        public static readonly string notValidModelEmployee = "Need to fill all information to create a Employee!";
        public static readonly string employeeDoesNotExist = "Employee with that Id: {0} does not exist!";
        public static readonly string failToAuthenticateEmployee = "Employee with email: {0}, fail to authenticate!";

        // WarehouseService
        public static readonly string notValidModelWarehouse = "Need to fill all information to create a Warehouse!";
        public static readonly string notValidName = "Warehouse that try to create is not valid!";
        public static readonly string warehouseDoesNotExist = "Warehouse with that Id: {0} does not exist!";

        // ShipmentService
        public static readonly string messageNotExist = "Shipment with ID: {0} does not exist.";
        public static readonly string messageNoContent = "No Content";
        public static readonly string messageErrorCreation = "Before proceeding with Shipment creation, need to be created at least two statuses.";
        public static readonly string messageSameStatus = "Shipment is already with status: {0}.";
        public static readonly string messageStatusTableEmpty = "Status table is empty.";
        public static readonly string messageNotValidStatus = "Shipment status that has been entered is not valid.";
        public static readonly string messageStatusAlreadyCompleted = "Shipment is already with status: Completed.";
        public static readonly string messageFindWithoutParam = "Try to search without parameter (search by ?warehouse=\"name\" or by ?customer=\"name\").";
        public static readonly string messageFindWithTwoParams = "Only one param at a time is allowed.";
        public static readonly string messageEmptyShipmentError = "Shipment is empty, can be changed to status On the way";
        public static readonly string notExistParsel = "Parcel with Tracking number: {0} does not exist.";
        public static readonly string notExistShipment = "Shipment with Tracking number: {0} does not exist.";
        public static readonly string notExistWarehouse = "Warehouse with name: {0} does not exist.";
        public static readonly string lackOfPermisionsParcelNotExist = "You dont have permissions/Parcel with that Tracking number does not exists!";
        public static readonly string parcelIsAlreadyInShipment = "Parcel with Tracking number: {0} is in the Shipment: {1}";
        public static readonly string parcelIsNotInShipment = "Parcel with Tracking number: {0} is not in the Shipment: {1}";
        public static readonly string notExistStatus = "Status with name: {0} does not exist!";

        // StatusService
        public static readonly string cantFindStatusWithId = "Status with id: {0} does not exists!";

        // Commons
        public static readonly string cantUpdate = "Permission denied: You do not have permission to update this account";
        public static readonly string cantCreate = "Permission denied: You do not have permission to create this account";
        public static readonly string inputParamsNotValid = "Not valid parameters";
        public static readonly string notValidInput = "Input parameter is not valid!";
        public static readonly string failToAuthenticate = "Permission denied: You fail to Authenticate.";

        // Parcels
        public static readonly string shipmentIsNotPreparing = "Shipment is Already \"On the way\" or \"Completed!\"";


        // Address
        public static readonly string novtValidAddressId = "Address with that id: {0} does not exists.";
    }
}
