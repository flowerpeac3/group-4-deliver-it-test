﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly DeliverITDbContext context;
        public WarehouseService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public WarehouseDTO Create(WarehouseDTO newWarehouse)
        {
            this.ValidateWarehouse(newWarehouse);

            var city = this.GetCountryCity(newWarehouse.CountryName, newWarehouse.CityName);

            var warehouse = new Warehouse
            {
                Name = newWarehouse.Name,
                Address = new Address
                {
                    StreetName = newWarehouse.StreetName,
                    City = city,
                }
            };

            this.context.Warehouses.Add(warehouse);
            this.context.SaveChanges();

            return new WarehouseDTO(warehouse);
        }

        public bool Delete(int id)
        {
            var warehouse = this.context.Warehouses
                .Include(w => w.Address)
                .FirstOrDefault(c => c.WarehouseId == id);
            var address = warehouse.Address;

            if (warehouse == null)
            {
                throw new ArgumentNullException(string.Format(Message.warehouseDoesNotExist, id));
            }

            address.IsDeleted = true;
            warehouse.IsDeleted = true;


            this.context.SaveChanges();

            return true;
        }

        public WarehouseDTO Get(int id)
        {
            var warehouse = this.context.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Shipment)
                        .ThenInclude(s => s.Status)
                .FirstOrDefault(w => w.WarehouseId == id && w.IsDeleted == false);

            if (warehouse == null)
            {
                throw new ArgumentNullException(string.Format(Message.warehouseDoesNotExist, id));
            }

            return new WarehouseDTO(warehouse);
        }

        public Warehouse GetByName(string name)
        {
            var warehouse = this.context.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(w => w.Parcels)
                .FirstOrDefault(w => w.Name.ToLower().Equals(name.ToLower()) && w.IsDeleted == false);

            if (warehouse == null)
            {
                throw new ArgumentNullException(string.Format(Message.warehouseDoesNotExist, name));
            }

            return warehouse;
        }

        public IEnumerable<WarehouseDTO> GetAll()
        {
            List<Warehouse> warehouses = new List<Warehouse>();

            warehouses = this.context.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Category)
                .Include(w => w.Parcels)
                    .ThenInclude(p => p.Shipment)
                        .ThenInclude(s => s.Status)
                .Where(w => w.IsDeleted == false)
                .ToList();

            List<WarehouseDTO> warehousesResult = new List<WarehouseDTO>();

            foreach (var item in warehouses)
            {
                warehousesResult.Add(new WarehouseDTO(item));
            }

            return warehousesResult;
        }

        public WarehouseDTO Update(int id, WarehouseDTO updateWarehouse)
        {
            var warehouse = this.context.Warehouses
            .Include(w => w.Address)
                .ThenInclude(a => a.City)
                .ThenInclude(c => c.Country)
            .FirstOrDefault(w => w.WarehouseId == id && w.IsDeleted == false)
            ?? throw new ArgumentException(string.Format(Message.warehouseDoesNotExist, id));

            if (updateWarehouse == null)
                throw new ArgumentException(Message.notValidModelWarehouse);

            if (updateWarehouse.CountryName == null
                && updateWarehouse.CityName == null || updateWarehouse.CityName.Length < 3)
            {
                throw new ArgumentException(Message.notValidCountryOtCity);
            }

            if (updateWarehouse.StreetName.Length < 5)
            {
                throw new ArgumentException(Message.notValidAddress);
            }

            var city = GetCountryCity(updateWarehouse.CountryName, updateWarehouse.CityName);

            warehouse.Address.StreetName = updateWarehouse.StreetName ?? warehouse.Address.StreetName;
            warehouse.Address.City = city ?? warehouse.Address.City;
            warehouse.Name = updateWarehouse.Name ?? warehouse.Name;

            this.context.SaveChanges();

            return new WarehouseDTO(warehouse);
        }

        public IEnumerable<WarehouseLocationDTO> GetAllLocations()
        {
            return this.context.Warehouses
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country)
                .Where(w => w.IsDeleted == false)
                .Select(w => new WarehouseLocationDTO(w));
        }

        private City GetCountryCity(string countryName, string cityName)
        {
            var country = this.context.Countries.FirstOrDefault(c => c.Name.ToLower().Equals(countryName.ToLower()))
                                ?? new Country { Name = countryName };

            var city = this.context.Cities
                .FirstOrDefault(c => c.Name.ToLower().Equals(cityName.ToLower())
                    && c.Country.Name.ToLower().Equals(countryName.ToLower()))
                ?? new City()
                {
                    Name = cityName,
                    Country = country,
                };

            return city;
        }

        private bool ValidateWarehouse(WarehouseDTO warehouse)
        {
            if (warehouse == null)
                throw new ArgumentException(Message.notValidCustomerModel);

            if (warehouse.Name == null || warehouse.Name.Length < 3)
                throw new ArgumentException(Message.notValidName);

            if (warehouse.CityName == null || warehouse.CityName.Length < 3)
                throw new ArgumentException(Message.notValidCity);

            if (warehouse.CountryName == null)
                throw new ArgumentException(Message.notValidCountry);

            if (warehouse.StreetName == null || warehouse.StreetName.Length < 3)
                throw new ArgumentException(Message.notValidAddress);

            return true;
        }
    }
}
