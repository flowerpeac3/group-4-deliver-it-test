﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class AddressDTO
    {
        public AddressDTO(Address address)
        {
            this.Street = address.StreetName;
            this.City = address.City.Name;
            this.Country = address.City.Country.Name;
        }

        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
