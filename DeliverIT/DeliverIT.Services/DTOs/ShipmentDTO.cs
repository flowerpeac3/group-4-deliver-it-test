﻿using DeliverIT.Models;
using System;
using System.Collections.Generic;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentDTO
    {
        public ShipmentDTO()
        {

        }
        public ShipmentDTO(Shipment shipment)
        {
            this.ShipmentId = shipment.ShipmentId;
            this.Identificator = shipment.Identificator;
            this.DepartureDate = shipment.DepartureDate;
            this.ArrivalDate = shipment.ArrivalDate;
            this.Status = shipment.Status.Name;
            foreach (var item in shipment.Parcels)
            {
                if(item.IsDeleted == false)
                    this.Parcels.Add(new ParcelDescriptionDTO(item));
            }
        }

        public ShipmentDTO(Shipment shipment, string status)
        {
            this.ShipmentId = shipment.ShipmentId;
            this.Identificator = shipment.Identificator;
            this.DepartureDate = shipment.DepartureDate;
            this.ArrivalDate = shipment.ArrivalDate;
            this.Status = status;
            
            foreach (var item in shipment.Parcels)
            {
                this.Parcels.Add(new ParcelDescriptionDTO(item));
            }
        }

        public int ShipmentId { get; set; }
        public string Identificator { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public string Status { get; set; }
        public ICollection<ParcelDescriptionDTO> Parcels { get; set; } = new List<ParcelDescriptionDTO>();
    }
}
