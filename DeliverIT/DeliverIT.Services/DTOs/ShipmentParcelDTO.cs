﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentParcelDTO
    {
        public string Identificator { get; set; }

        public string ParcelTrackNumber { get; set; }
    }
}
