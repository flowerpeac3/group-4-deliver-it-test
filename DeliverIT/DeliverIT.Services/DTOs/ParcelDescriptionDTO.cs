﻿using DeliverIT.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ParcelDescriptionDTO
    {
        public ParcelDescriptionDTO()
        {

        }

        public ParcelDescriptionDTO(Parcel parcel)
        {

            this.Weight = parcel.Weight;
            this.ParcelTrackNumber = parcel.ParcelTrackNumber;
            this.CustomerName = parcel.Customer.FirstName + " " + parcel.Customer.LastName;
        }

        public double Weight { get; set; }
        public string ParcelTrackNumber { get; set; }
        public string CustomerName { get; set; }

    }
}
