﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class EmployeeDTO
    {
        public EmployeeDTO()
        {

        }

        public EmployeeDTO(Employee employee)
        {
            this.FirstName = employee.FirstName;
            this.LastName = employee.LastName;
            this.Email = employee.Email;
            this.StreetName = employee.Address.StreetName;
            this.CityName = employee.Address.City.Name;
            this.CountryName = employee.Address.City.Country.Name;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }
}
