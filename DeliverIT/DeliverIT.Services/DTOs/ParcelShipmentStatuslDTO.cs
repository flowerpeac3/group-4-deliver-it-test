﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ParcelShipmentStatuslDTO
    {
        public ParcelShipmentStatuslDTO(string number, string status)
        {
            this.ParcelTrackingNumber = number;
            this.Status = status;
        }
        public string ParcelTrackingNumber { get; set; }
        public string Status { get; set; }
    }
}
