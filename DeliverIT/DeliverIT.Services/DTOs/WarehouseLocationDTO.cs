﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class WarehouseLocationDTO
    {
        public WarehouseLocationDTO(Warehouse warehouse)
        {
            this.Name = warehouse.Name;
            this.StreetName = warehouse.Address.StreetName;
            this.CityName = warehouse.Address.City.Name;
            this.CountryName = warehouse.Address.City.Country.Name;
        }
        public string Name { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }
}
