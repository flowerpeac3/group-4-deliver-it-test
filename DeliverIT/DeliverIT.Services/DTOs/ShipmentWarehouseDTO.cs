﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentWarehouseDTO
    {
        public ShipmentWarehouseDTO(string warehouseName, IEnumerable<string> identificator)
        {
            this.WarehouseName = warehouseName;
            this.Identificators = identificator;
        }

        public string WarehouseName { get; set; }
        public IEnumerable<string> Identificators { get; set; }
        
    }
}
