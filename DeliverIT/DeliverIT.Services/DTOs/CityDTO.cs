﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class CityDTO
    {
        public string City { get; set; }
        public string Country { get; set; }

    }
}
