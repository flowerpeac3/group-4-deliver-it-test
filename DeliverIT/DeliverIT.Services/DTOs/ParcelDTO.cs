﻿using DeliverIT.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ParcelDTO
    {
        public ParcelDTO()
        {

        }

        public ParcelDTO(Parcel parcel)
        {
            this.Weight = parcel.Weight;
            this.CustomerName = string.Concat(parcel.Customer.FirstName + " " + parcel.Customer.LastName);
            this.ParcelTrackNumber = parcel.ParcelTrackNumber;

            if (parcel.Shipment != null)
            {
                this.ShipmentIdentificator = parcel.Shipment.Identificator;
                this.Status = parcel.Shipment.Status.Name;
            }

            this.EmployeeName = string.Concat(parcel.Employee.FirstName + " " + parcel.Employee.LastName);
            this.WarehouseName = parcel.Warehouse.Name;
            this.CategoryName = parcel.Category.Name;
        }

        public double Weight { get; set; }
        public string CustomerName { get; set; }
        public string ParcelTrackNumber { get; set; }
        public string ShipmentIdentificator { get; set; }
        public string Status { get; set; }
        public string EmployeeName { get; set; }
        public string WarehouseName { get; set; }
        public string CategoryName { get; set; }

    }

}
