﻿using DeliverIT.Models;
using System.Collections.Generic;

namespace DeliverIT.Services.DTOs
{
    public class WarehouseDTO
    {
        public WarehouseDTO()
        {

        }

        public WarehouseDTO(Warehouse warehouse)
        {
            this.Name = warehouse.Name;
            this.StreetName = warehouse.Address.StreetName;
            this.CityName = warehouse.Address.City.Name;
            this.CountryName = warehouse.Address.City.Country.Name;
            
            foreach (var item in warehouse.Parcels)
            {
                this.Parcels.Add(new ParcelDTO(item));
            }
            
        }

        public string Name { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public ICollection<ParcelDTO> Parcels { get; set; } = new List<ParcelDTO>();
    }
}
