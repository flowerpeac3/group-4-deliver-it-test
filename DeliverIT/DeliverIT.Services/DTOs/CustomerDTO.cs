﻿using DeliverIT.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class CustomerDTO
    {
        public CustomerDTO()
        {

        }

        public CustomerDTO(Customer customer)
        {
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.Email = customer.Email;
            this.StreetName = customer.AddressForDelivery.StreetName;
            this.CityName = customer.AddressForDelivery.City.Name;
            this.CountryName = customer.AddressForDelivery.City.Country.Name;

            foreach (var item in customer.Parcels)
            {
                this.Parcels.Add(new ParcelDTO(item));
            }
         
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public ICollection<ParcelDTO> Parcels { get; set; } = new List<ParcelDTO>();
    }
}
