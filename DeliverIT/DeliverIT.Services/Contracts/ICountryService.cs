﻿using DeliverIT.Models;
using System.Collections.Generic;

namespace DeliverIT.Services
{
    public interface ICountryService
    {
        IEnumerable<Country> GetAll();
        Country Get(int id);
        Country Create(Country newCountry);
        Country Update(int id, string newName);
        bool Delete(int id);
    }
}
