﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IParcelService
    {
        IEnumerable<ParcelDTO> GetAll();
        ParcelDTO Get(int id);
        Parcel Get(string trackNumber);
        ParcelDTO Create(ParcelDTO newParcel);
        ParcelDTO Update(int id, ParcelDTO newParcel);
        bool Delete(int id);

        IEnumerable<ParcelDTO> FIlterByCustomerFirstName(string name);
        IEnumerable<ParcelDTO> FIlterByCustomerLastName(string name);
        IEnumerable<ParcelDTO> FIlterByWarehouse(string name);
        IEnumerable<ParcelDTO> FIlterByCategory(string name);
        IEnumerable<ParcelDTO> FIlterByWeight(double weight);
        //IEnumerable<ParcelDTO> ShowAllParcels(string customerName);
        IEnumerable<ParcelDTO> PastParcels(string customerName);
        IEnumerable<ParcelDTO> ParcelsOnTheWay(string customerName);
    }
}
