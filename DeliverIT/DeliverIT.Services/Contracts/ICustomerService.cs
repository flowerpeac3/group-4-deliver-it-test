﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Contracts
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDTO> GetAll();
        CustomerDTO Get(int id);
        Customer GetCustomerByEmail(string email);
        Customer GetCustomerByFirstName(string firstName);
        Customer GetCustomerByFullName(string firstName, string lastName);
        CustomerDTO Create(CustomerDTO newCustomer);
        CustomerDTO Update(int id, CustomerDTO newCustomer);
        bool Delete(int id);
        int CustomersCounter();
        IEnumerable<CustomerDTO> Search(string email, string fname, string lname, string searchByparcel);
    }
}
