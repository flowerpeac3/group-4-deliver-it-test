﻿using DeliverIT.Models;
using System.Collections.Generic;

namespace DeliverIT.Services
{
    public interface ICityService
    {
        IEnumerable<City> GetAll();
        City Get(int id);
        City Create(City newCity);
        City Update(int id, string newName, int newCountry);
        bool Delete(int id);
    }
}
