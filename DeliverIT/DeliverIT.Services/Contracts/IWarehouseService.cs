﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services
{
    public interface IWarehouseService
    {
        IEnumerable<WarehouseDTO> GetAll();
        WarehouseDTO Get(int id);
        Warehouse GetByName(string name);
        WarehouseDTO Create(WarehouseDTO newWarehouse);
        WarehouseDTO Update(int id, WarehouseDTO updateWarehouse);
        IEnumerable<WarehouseLocationDTO> GetAllLocations();
        bool Delete(int id);
    }
}
