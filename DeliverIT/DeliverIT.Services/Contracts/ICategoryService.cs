﻿using DeliverIT.Models;
using System.Collections.Generic;

namespace DeliverIT.Services.Contracts
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetAll();
        Category Get(int id);
        Category GetByName(string name);
        Category Create(Category newCategory);
        Category Update(int id, string newName);
        bool Delete(int id);
    }
}
