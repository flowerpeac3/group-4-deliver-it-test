﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;

namespace DeliverIT.Services
{
    public interface IShipmentService
    {
        IEnumerable<ShipmentDTO> GetAll();
        ShipmentDTO Get(int id);
        Shipment GetShipmentByIdentificator(string identificator);
        IEnumerable<ShipmentDTO> GetByStatus(string status);
        List<ShipmentDTO> GetCustomerParcels(int id);
        ParcelShipmentStatuslDTO GetCustomerParcelStatus(int id, string parcelTrackingNumber);

        ShipmentWarehouseDTO GetWarehouseShipmentsStatuses(string warehouseName);
        IEnumerable<ShipmentDTO> Filter(string warehouse, string email);
        ShipmentDTO AddParcelToShipment(ShipmentParcelDTO shipmentParcel);
        ShipmentDTO RemoveParcelToShipment(ShipmentParcelDTO shipmentParcel);
        ShipmentDTO Create();
        ShipmentDTO Update(int id, ShipmentDTO updateShipment);
        bool Delete(int id);
    }
}
