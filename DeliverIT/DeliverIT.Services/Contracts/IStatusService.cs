﻿using DeliverIT.Models;
using System.Collections.Generic;

namespace DeliverIT.Services.Contracts
{
    public interface IStatusService
    {
        IEnumerable<Status> GetAll();
        Status Get(int id);
        Status GetStatusByName(string name);
        Status Create(Status newStatus);
        Status Update(int id, string newName);
        bool Delete(int id);
    }
}
