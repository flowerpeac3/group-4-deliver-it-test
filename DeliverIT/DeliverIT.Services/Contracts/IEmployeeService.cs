﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDTO> GetAll();
        EmployeeDTO Get(int id);
        Employee GetEmployeeByEmail(string email);
        Employee GetEmployeeByName(string firstName);
        EmployeeDTO Create(Employee employeeEditor, EmployeeDTO newCustomer);
        EmployeeDTO Update(int id, EmployeeDTO newCustomer);
        bool Delete(int id);
    }
}
