﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Contracts
{
    public interface IAddressService
    {
        IEnumerable<AddressDTO> GetAll();
        AddressDTO Get(int id);
        Address Create(Address newAddress);
        Address Update(int id, Address newAddress);
        bool Delete(int id);
    }
}
