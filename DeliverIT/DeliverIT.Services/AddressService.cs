﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class AddressService : IAddressService
    {
        private readonly DeliverITDbContext context;
        public AddressService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public Address Create(Address newAddress)
        {

            this.context.Addresses.Add(newAddress);
            this.context.SaveChanges();

            return newAddress;
        }

        public bool Delete(int id)
        {
            var address = this.context.Addresses.FirstOrDefault(a => a.AddressId == id);
            if (address == null)
            {
                throw new ArgumentNullException(string.Format(Message.novtValidAddressId, id));
            }

            address.IsDeleted = true;

            this.context.SaveChanges();

            return true;
        }

        public AddressDTO Get(int id)
        {
            var address = this.context.Addresses
                .Include(a => a.City)
                    .ThenInclude(c => c.Country)
                .FirstOrDefault(a => a.AddressId == id);

            if (address == null)
            {
                throw new ArgumentNullException();
            }

            return new AddressDTO(address);
        }

        public IEnumerable<AddressDTO> GetAll()
        {
            var adresses = this.context.Addresses
                .Include(a => a.City)
                    .ThenInclude(c => c.Country);

            var addressesDTO = new List<AddressDTO>();

            foreach (var item in adresses)
            {
                addressesDTO.Add(new AddressDTO(item));
            }

            return addressesDTO;
        }

        public Address Update(int id, Address newAddress)
        {
            var address = this.context.Addresses.FirstOrDefault(a => a.AddressId == id);

            if (address == null)
            {
                throw new ArgumentNullException(string.Format(Message.novtValidAddressId, id));
            }

            address.StreetName = newAddress.StreetName ?? address.StreetName;
            address.CityId = newAddress.CityId;
            this.context.SaveChanges();

            return address;

        }
    }
}
