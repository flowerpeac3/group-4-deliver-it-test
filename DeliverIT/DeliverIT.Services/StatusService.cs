﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    public class StatusService : IStatusService
    {
        private readonly DeliverITDbContext context;
        public StatusService(DeliverITDbContext context)
        {
            this.context = context;
        }

        public Status Create(Status newStatus)
        {
            if (string.IsNullOrEmpty(newStatus.Name))
                throw new ArgumentException(Message.notValidInput);

            this.context.Statuses.Add(newStatus);
            
            this.context.SaveChanges();

            return newStatus;
        }

        public bool Delete(int id)
        {
            var status = this.context.Statuses
                .FirstOrDefault(c => c.StatusId == id)
                ?? throw new ArgumentException(string.Format(Message.cantFindStatusWithId, id));

            var deleted = this.context.Statuses.Remove(status);
            
            this.context.SaveChanges();

            if (deleted == null)
                return false;

            return true;
        }

        public Status Get(int id)
        {
            var status = this.context.Statuses.FirstOrDefault(c => c.StatusId == id);

            if (status == null)
            {
                throw new ArgumentNullException();
            }

            return status;
        }

        public Status GetStatusByName(string name)
        {
            if (name == null)
                throw new ArgumentException(Message.notValidInput);

            var status = this.context.Statuses.FirstOrDefault(c => c.Name.ToLower().Equals(name.ToLower()));

            if (status == null)
            {
                throw new ArgumentNullException();
            }

            return status;
        }

        public IEnumerable<Status> GetAll()
        {
            return this.context.Statuses;
        }

        public Status Update(int id, string newName)
        {
            if (string.IsNullOrEmpty(newName))
                throw new ArgumentException(Message.notValidInput);

            var status = this.context.Statuses.FirstOrDefault(c => c.StatusId == id);

            if (status == null)
            {
                throw new ArgumentNullException();
            }

            status.Name = newName;
            
            this.context.SaveChanges();

            return status;
        }
    }
}
