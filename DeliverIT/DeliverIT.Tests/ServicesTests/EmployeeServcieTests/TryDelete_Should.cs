﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryDelete_Should
    {
        [TestMethod]
        public void DeleteEmployeeCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(DeleteEmployeeCorrect));
            var sut = new EmployeeService(context);
            var allEmployeesActual = context.Employees;
            var employee = allEmployeesActual.FirstOrDefault();
            var employeeIsDelete = employee.IsDeleted;

            //Act
            var result = sut.Delete(employee.EmployeeId);
            var employeeExpected = context.Employees.FirstOrDefault();

            //Assert
            Assert.AreNotEqual(employeeIsDelete, employeeExpected.IsDeleted);
        }

        [TestMethod]
        public void DeleteEmployeeThrowExceptionNotValidId()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(DeleteEmployeeThrowExceptionNotValidId));
            var sut = new EmployeeService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Delete(111));
        }
    }
}
