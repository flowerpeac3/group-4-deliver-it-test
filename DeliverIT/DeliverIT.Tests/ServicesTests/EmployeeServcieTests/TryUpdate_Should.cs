﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryUpdate_Should
    {
        [TestMethod]
        public void UpdateEmployeeCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateEmployeeCorrect));
            var sut = new EmployeeService(context);
            var employeeActual = context.Employees.FirstOrDefault();
            // Test customer
            //EmployeeId = 1,
            //FirstName = "Petar",
            //LastName = "Ivanov",
            //Email = "pivanov@gmail.com",
            //Address = 1
            //StreetName = "12 North West Lane",
            //CityName = "Los Angeles",
            //CountryName = "USA"

            var employeeDTO = new EmployeeDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "4321 East Road",
                CityName = "Seattle",
                CountryName = "USA"
            };


            //Act
            var result = sut.Update(employeeActual.EmployeeId, employeeDTO);

            //Assert
            Assert.AreEqual(employeeDTO.FirstName, result.FirstName);
            Assert.AreEqual(employeeDTO.LastName, result.LastName);
            Assert.AreEqual(employeeDTO.Email, result.Email);
            Assert.AreEqual(employeeDTO.StreetName, result.StreetName);
            Assert.AreEqual(employeeDTO.CityName, result.CityName);
            Assert.AreEqual(employeeDTO.CountryName, result.CountryName);
        }

        [TestMethod]
        public void UpdateEmployeeWithExistingEmailException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateEmployeeWithExistingEmailException));
            var sut = new EmployeeService(context);
            var employeeActual = context.Employees.FirstOrDefault();
            var employeeDTO = new EmployeeDTO
            {
                Email = "mpraznikov@vremeto.bg"
            };


            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(employeeActual.EmployeeId, employeeDTO));
        }

        [TestMethod]
        public void UpdateEmployeeWithNullInputException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateEmployeeWithNullInputException));
            var sut = new EmployeeService(context);
            var employeeActual = context.Employees.FirstOrDefault();

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(employeeActual.EmployeeId, null));
        }

        [TestMethod]
        public void UpdateEmployeeAlreadyDeletedException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateEmployeeAlreadyDeletedException));
            var sut = new EmployeeService(context);
            var employeeActual = context.Employees.FirstOrDefault();
            employeeActual.IsDeleted = true;
            var employeeDTO = new EmployeeDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(employeeActual.EmployeeId, employeeDTO));
        }
    }
}
