﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryGet_Should
    {
        [TestMethod]
        public void ReturnCorrectEmployee()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnCorrectEmployee));
            var actual = new EmployeeDTO(context.Employees.FirstOrDefault(c => c.EmployeeId == 1));
            var sut = new EmployeeService(context);

            //Act
            var result = sut.Get(1);

            //Assert
            Assert.AreEqual(actual.StreetName, result.StreetName);
            Assert.AreEqual(actual.Email, result.Email);
            Assert.AreEqual(actual.FirstName, result.FirstName);
            Assert.AreEqual(actual.LastName, result.LastName);
            Assert.AreEqual(actual.CityName, result.CityName);
            Assert.AreEqual(actual.CountryName, result.CountryName);
        }

        [TestMethod]
        public void ReturnEmployeeNull()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnEmployeeNull));
            var sut = new EmployeeService(context);

            //Act
            var result = sut.Get(111);

            //Assert
            Assert.IsNull(result);
        }
    }
}
