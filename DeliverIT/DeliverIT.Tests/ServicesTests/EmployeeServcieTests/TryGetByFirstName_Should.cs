﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryGetByFirstName_Should
    {
        [TestMethod]
        public void GetEmployeeByNameEmptyInputString()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetEmployeeByNameEmptyInputString));
            var sut = new EmployeeService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetEmployeeByName(null));
        }

        [TestMethod]
        public void GetEmployeeByNameNonexisting()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetEmployeeByNameNonexisting));
            var sut = new EmployeeService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetEmployeeByName("FakeName"));
        }

        [TestMethod]
        public void GetEmployeeByNameCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetEmployeeByNameCorrect));
            var sut = new EmployeeService(context);
            var employee = context.Employees.FirstOrDefault();

            //Act
            var result = sut.GetEmployeeByName(employee.FirstName);

            //Assert
            Assert.AreEqual(employee.Email, result.Email);
            Assert.AreEqual(employee.FirstName, result.FirstName);
            Assert.AreEqual(employee.LastName, result.LastName);
            Assert.AreEqual(employee.IsDeleted, result.IsDeleted);
        }
    }
}
