﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryGetByEmail_Should
    {
        [TestMethod]
        public void GetEmployeeByEmailEmptyInputString()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetEmployeeByEmailEmptyInputString));
            var sut = new EmployeeService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetEmployeeByEmail(null));
        }

        [TestMethod]
        public void GetEmployeeByEmailCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetEmployeeByEmailCorrect));
            var sut = new EmployeeService(context);
            var employee = context.Employees.FirstOrDefault();

            //Act
            var result = sut.GetEmployeeByEmail(employee.Email);

            //Assert
            Assert.AreEqual(employee.Email, result.Email);
            Assert.AreEqual(employee.FirstName, result.FirstName);
            Assert.AreEqual(employee.LastName, result.LastName);
            Assert.AreEqual(employee.IsDeleted, result.IsDeleted);
        }
    }
}
