﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryCreate_Should
    {
        [TestMethod]
        public void CreateEmployeeCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(CreateEmployeeCorrect));
            var sut = new EmployeeService(context);
            var allEmplyeesActual = context.Employees;
            var employeeEditor = context.Employees.FirstOrDefault(e => e.EmployeeId == 1);

            var employeeDTO = new EmployeeDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };


            //Act
            var result = sut.Create(employeeEditor, employeeDTO);

            //Assert
            Assert.AreEqual(employeeDTO.StreetName, result.StreetName);
            Assert.AreEqual(employeeDTO.Email, result.Email);
            Assert.AreEqual(employeeDTO.FirstName, result.FirstName);
            Assert.AreEqual(employeeDTO.LastName, result.LastName);
            Assert.AreEqual(employeeDTO.CityName, result.CityName);
            Assert.AreEqual(employeeDTO.CountryName, result.CountryName);
        }

        [TestMethod]
        public void CreateEmployeeFailEmail()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(CreateEmployeeFailEmail));
            var sut = new EmployeeService(context);
            var employeeEditor = context.Employees.FirstOrDefault(e => e.EmployeeId == 1);

            var employeeDTO = new EmployeeDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "mpraznikov@vremeto.bg",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Create(employeeEditor, employeeDTO));

        }
    }
}
