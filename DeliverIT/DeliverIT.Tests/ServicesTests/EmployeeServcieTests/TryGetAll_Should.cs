﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.EmployeeServiceTests
{
    [TestClass]
    public class TryGetAll_Should
    {
        [TestMethod]
        public void ReturnAllEmployeesCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnAllEmployeesCorrect));
            var sut = new EmployeeService(context);
            var allEmployeesActual = context.Employees;

            //Act
            var result = sut.GetAll();

            //Assert
            Assert.AreEqual(result.Count(), allEmployeesActual.Count());
        }
    }
}
