﻿using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace DeliverIT.Tests.ServicesTests.AddressServiceTests
{
    [TestClass]
    public class TryGetAddress_Should
    {
        [TestMethod]
        public void Return_Correct_Address()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Address));
            var address = new Address { 
                AddressId = 1, 
                CityId = 2, StreetName = "Luben Karavelov 1",
                City = new City() { Country = new Country() },
                Customer = new Customer(), 
                Employee = new Employee(), 
                Warehouse = new Warehouse()
            };

            var addressDTO = new AddressDTO(address);

            using (var arrangeContext = new DeliverITDbContext(options))
            {
                arrangeContext.Addresses.Add(address);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DeliverITDbContext(options))
            {
                var sut = new AddressService(actContext);
                var result = sut.Get(1);

                //Assert
                Assert.AreEqual(addressDTO.Street, result.Street);
                Assert.AreEqual(addressDTO.City, result.City);
                Assert.AreEqual(addressDTO.Country, result.Country);
            }
        }

        [TestMethod]
        public void Return_Null_Address()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Null_Address));

            //Act & Assert
            using (var context = new DeliverITDbContext(options))
            {
                var sut = new AddressService(context);

                Assert.ThrowsException<ArgumentNullException>(() => sut.Get(1));
            }
        }

        
    }
}
