﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.ParcelServiceTests
{
    [TestClass]
    public class TryGet_Should
    {
        [TestMethod]
        public void ReturnCorrectParcel()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnCorrectParcel));
            var actual = new ParcelDTO(context.Parcels.FirstOrDefault(c => c.ParcelId == 1));
            var sut = new ParcelService(context);

            //Act
            var result = sut.Get(1);

            //Arrange
            Assert.AreEqual(actual.CustomerName, result.CustomerName);
            Assert.AreEqual(actual.EmployeeName, result.EmployeeName);
            Assert.AreEqual(actual.WarehouseName, result.WarehouseName);
            Assert.AreEqual(actual.Weight, result.Weight);
            Assert.AreEqual(actual.ParcelTrackNumber, result.ParcelTrackNumber);
            Assert.AreEqual(actual.CategoryName, result.CategoryName);
            Assert.AreEqual(actual.Status, result.Status);
        }

        [TestMethod]
        public void ReturnCorrectNull()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnCorrectNull));
            var sut = new CustomerService(context);

            //Act
            var result = sut.Get(1000);

            //Assert
            Assert.IsNull(result);
        }
    }
}
