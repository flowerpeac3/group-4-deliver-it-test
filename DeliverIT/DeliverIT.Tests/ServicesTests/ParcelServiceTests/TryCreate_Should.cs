﻿using DeliverIT.Database;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.ParcelServiceTests
{
    [TestClass]
    public class TryCreate_Should
    {
        [TestMethod]
        public void CreateCorrectParcel()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(CreateCorrectParcel));
            var employeeService = new EmployeeService(context);
            var status = new StatusService(context);
            var shipmentService = new ShipmentService(context, status);
            var customerService = new CustomerService(context);
            var warehouseService = new WarehouseService(context);
            var categoryService = new CategoryService(context);

            var sut = new ParcelService(context, shipmentService, customerService, employeeService, warehouseService, categoryService);



            var parcelDTO = new ParcelDTO
            {
                CustomerName = context.Customers.FirstOrDefault().FirstName +  " " + context.Customers.FirstOrDefault().LastName,
                EmployeeName = context.Employees.FirstOrDefault().FirstName + " " + context.Employees.FirstOrDefault().LastName,
                WarehouseName = context.Warehouses.FirstOrDefault().Name,
                CategoryName = context.Categories.FirstOrDefault().Name,
                ShipmentIdentificator = context.Shipments.FirstOrDefault().Identificator,
                Weight = 11
            };
            // Act
            var result = sut.Create(parcelDTO);

            //Arrange
            Assert.AreEqual(parcelDTO.CustomerName, result.CustomerName);
            Assert.AreEqual(parcelDTO.EmployeeName, result.EmployeeName);
            Assert.AreEqual(parcelDTO.WarehouseName, result.WarehouseName);
            Assert.AreEqual(parcelDTO.CategoryName, result.CategoryName);
            Assert.AreEqual(parcelDTO.ShipmentIdentificator, result.ShipmentIdentificator);
            Assert.AreEqual(parcelDTO.Weight, result.Weight);
        }


    }
}
