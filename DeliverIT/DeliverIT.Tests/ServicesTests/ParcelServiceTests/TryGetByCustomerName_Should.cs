﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.ParcelServiceTests
{
    [TestClass]
    public class TryGetByCustomerName_Should
    {
        [TestMethod]
        public void GetByCustomerFirstName()
        {
            var context = Utils.GetContext(nameof(GetByCustomerFirstName));
            var actual = context.Parcels.Where(c => c.Customer.FirstName == "Ivan");
            var sut = new ParcelService(context);

            //Act
            
            var result = sut.FIlterByCustomerFirstName("Ivan");

            Assert.AreEqual(actual.Count(), result.Count());
        }
        [TestMethod]
        public void GetCustomerByLastName()
        {
            var context = Utils.GetContext(nameof(GetCustomerByLastName));
            var actual = context.Parcels.Where(c => c.Customer.LastName == "Ivanov");
            var sut = new ParcelService(context);

            //Act

            var result = sut.FIlterByCustomerLastName("Ivanov");

            Assert.AreEqual(actual.Count(), result.Count());
        }
    }
}
