﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.ParcelServiceTests
{
    [TestClass]
    public class TryGetAll_Should
    {
        [TestMethod]
        public void ReturnAllParcelCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnAllParcelCorrect));
            var sut = new ParcelService(context);
            var allParcels = context.Parcels;

            //Act
            var result = sut.GetAll();

            //Assert
            Assert.AreEqual(result.Count(), allParcels.Count());
        }
    }
}
