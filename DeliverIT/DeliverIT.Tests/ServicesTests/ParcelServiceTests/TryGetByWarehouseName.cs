﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.ParcelServiceTests
{
    [TestClass]
    public class TryGetByWarehouseName
    {
        [TestMethod]
        public void GetByWarehouseName()
        {
            var context = Utils.GetContext(nameof(GetByWarehouseName));
            var actual = context.Parcels.Where(c => c.Warehouse.Name == "Kometa");
            var sut = new ParcelService(context);

            //Act
            var result = sut.FIlterByWarehouse("Kometa");

            Assert.AreEqual(actual.Count(), result.Count());
        }
    }
}
