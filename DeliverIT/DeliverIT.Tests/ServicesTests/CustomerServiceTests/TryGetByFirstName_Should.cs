﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryGetByFirstName_Should
    {
        [TestMethod]
        public void GetCustomerByFirstNameEmptyInputString()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFirstNameEmptyInputString));
            var sut = new CustomerService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetCustomerByFirstName(null));
        }

        [TestMethod]
        public void GetCustomerByFirstNameNonexisting()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFirstNameNonexisting));
            var sut = new CustomerService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetCustomerByFirstName("FakeName"));
        }

        [TestMethod]
        public void GetCustomerByFirstNameCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFirstNameCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();

            //Act
            var result = sut.GetCustomerByFirstName(customer.FirstName);

            //Assert
            Assert.AreEqual(customer.Email, result.Email);
            Assert.AreEqual(customer.FirstName, result.FirstName);
            Assert.AreEqual(customer.LastName, result.LastName);
            Assert.AreEqual(customer.IsDeleted, result.IsDeleted);
        }
    }
}
