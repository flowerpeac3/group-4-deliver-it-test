﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryGetByFullName_Should
    {
        [TestMethod]
        public void GetCustomerByFullNameCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFullNameCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();

            //Act
            var result = sut.GetCustomerByFullName(customer.FirstName, customer.LastName);

            //Assert
            Assert.AreEqual(customer.Email, result.Email);
            Assert.AreEqual(customer.FirstName, result.FirstName);
            Assert.AreEqual(customer.LastName, result.LastName);
            Assert.AreEqual(customer.IsDeleted, result.IsDeleted);
        }

        [TestMethod]
        public void GetCustomerByFullNameNullInput()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFullNameNullInput));
            var sut = new CustomerService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetCustomerByFullName(null, null));
        }

        [TestMethod]
        public void GetCustomerByFullNameNotExisting()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByFullNameNotExisting));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetCustomerByFullName("FakeFisrtName", "FakeLastName"));
        }
    }
}
