﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryGet_Should
    {
        [TestMethod]
        public void ReturnCorrectCustomer()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnCorrectCustomer));
            var actual = new CustomerDTO(context.Customers.FirstOrDefault(c => c.CustomerId == 1));
            var sut = new CustomerService(context);

            //Act
            var result = sut.Get(1);

            //Assert
            Assert.AreEqual(actual.StreetName, result.StreetName);
            Assert.AreEqual(actual.Email, result.Email);
            Assert.AreEqual(actual.FirstName, result.FirstName);
            Assert.AreEqual(actual.LastName, result.LastName);
            Assert.AreEqual(actual.CityName, result.CityName);
            Assert.AreEqual(actual.CountryName, result.CountryName);
        }

        [TestMethod]
        public void ReturnNull()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnNull));
            var sut = new CustomerService(context);

            //Act
            var result = sut.Get(111);

            //Assert
            Assert.IsNull(result);
        }
    }
}
