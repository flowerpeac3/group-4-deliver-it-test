﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryCounter_Should
    {
        [TestMethod]
        public void GetCustomerCounter()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerCounter));
            var sut = new CustomerService(context);
            var counterActual = context.Customers.Count();

            //Act
            var result = sut.CustomersCounter();

            //Assert
            Assert.AreEqual(result, counterActual);

        }
    }
}
