﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryGetAll_Should
    {
        [TestMethod]
        public void ReturnAllCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(ReturnAllCorrect));
            var sut = new CustomerService(context);
            var allCustomersActual = context.Customers;

            //Act
            var result = sut.GetAll();

            //Assert
            Assert.AreEqual(result.Count(), allCustomersActual.Count());
        }
    }
}
