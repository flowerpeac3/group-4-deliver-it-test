﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryCreate_Should
    {
        [TestMethod]
        public void CreateCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(CreateCorrect));
            var sut = new CustomerService(context);
            var allCustomersActual = context.Customers;

            var customerDTO = new CustomerDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };


            //Act
            var result = sut.Create(customerDTO);

            //Assert
            Assert.AreEqual(customerDTO.StreetName, result.StreetName);
            Assert.AreEqual(customerDTO.Email, result.Email);
            Assert.AreEqual(customerDTO.FirstName, result.FirstName);
            Assert.AreEqual(customerDTO.LastName, result.LastName);
            Assert.AreEqual(customerDTO.CityName, result.CityName);
            Assert.AreEqual(customerDTO.CountryName, result.CountryName);
        }

        [TestMethod]
        public void CreateFailEmail()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(CreateFailEmail));
            var sut = new CustomerService(context);
            var allCustomersActual = context.Customers;

            var customerDTO = new CustomerDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "ggg@gmail.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Create(customerDTO));

        }
    }
}
