﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryGetByEmail_Should
    {
        [TestMethod]
        public void GetCustomerByEmailEmptyInputString()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByEmailEmptyInputString));
            var sut = new CustomerService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetCustomerByEmail(null));
        }

        [TestMethod]
        public void GetCustomerByEmailCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(GetCustomerByEmailCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();

            //Act
            var result = sut.GetCustomerByEmail(customer.Email);

            //Assert
            Assert.AreEqual(customer.Email, result.Email);
            Assert.AreEqual(customer.FirstName, result.FirstName);
            Assert.AreEqual(customer.LastName, result.LastName);
            Assert.AreEqual(customer.IsDeleted, result.IsDeleted);
        }
    }
}
