﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryDelete_Should
    {
        [TestMethod]
        public void DeleteCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(DeleteCorrect));
            var sut = new CustomerService(context);
            var allCustomersActual = context.Customers;
            var customer = allCustomersActual.FirstOrDefault();
            var customerIsDelete = customer.IsDeleted;

            //Act
            var result = sut.Delete(customer.CustomerId);
            var customerExpected = context.Customers.FirstOrDefault();

            //Assert
            Assert.AreNotEqual(customerIsDelete, customerExpected.IsDeleted);
        }

        [TestMethod]
        public void DeleteThrowExceptionNotValidId()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(DeleteThrowExceptionNotValidId));
            var sut = new CustomerService(context);

            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.Delete(111));
        }
    }
}
