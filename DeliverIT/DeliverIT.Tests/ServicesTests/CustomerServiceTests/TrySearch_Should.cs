﻿using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TrySearch_Should
    {

        [TestMethod]
        public void SearchByEmailCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(SearchByEmailCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();
            var countersActual = context.Customers.Where(c => c.Email.ToLower().Contains(customer.Email.ToLower()) && c.IsDeleted == false).Count();

            //Act
            var result = sut.Search(customer.Email, null, null, null);

            //Assert
            Assert.AreEqual(result.Count(), countersActual);
        }

        [TestMethod]
        public void SearchByFirstNameCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(SearchByFirstNameCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();
            var countersActual = context.Customers.Where(c => c.FirstName.ToLower().Contains(customer.FirstName.ToLower()) && c.IsDeleted == false).Count();

            //Act
            var result = sut.Search(null, customer.FirstName, null, null);

            //Assert
            Assert.AreEqual(result.Count(), countersActual);
        }

        [TestMethod]
        public void SearchByLastNameCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(SearchByLastNameCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();
            var countersActual = context.Customers.Where(c => c.LastName.ToLower().Contains(customer.LastName.ToLower()) && c.IsDeleted == false).Count();

            //Act
            var result = sut.Search(null, null, customer.LastName, null);

            //Assert
            Assert.AreEqual(result.Count(), countersActual);
        }

        [TestMethod]
        public void SearchWithNullInput()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(SearchWithNullInput));
            var sut = new CustomerService(context);


            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Search(null, null, null, null));
        }

        [TestMethod]
        public void SearchByParcelsCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(SearchByParcelsCorrect));
            var sut = new CustomerService(context);
            var customer = context.Customers.FirstOrDefault();
            var parcelsActual = context.Parcels.Where(p => p.CustomerId == customer.CustomerId).Count();

            //Act
            var result = sut.Search(null, customer.FirstName, customer.LastName, "yes").FirstOrDefault();

            //Assert
            Assert.AreEqual(result.Parcels.Count(), parcelsActual);
        }
    }
}
