﻿using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DeliverIT.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class TryUpdate_Should
    {
        [TestMethod]
        public void UpdateCorrect()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateCorrect));
            var sut = new CustomerService(context);
            var customerActual = context.Customers.FirstOrDefault();
            // Test customer
            //CustomerId = 1,
            //FirstName = "Gonzo",
            //LastName = "ivanov",
            //Email = "ggg@gmail.com",
            //AddressForDeliveryId = 1
            //StreetName = "Luben Karavelov 1",
            //CityName = "Varna",
            //CountryName = "Bulgaria"

            var customerDTO = new CustomerDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };


            //Act
            var result = sut.Update(customerActual.CustomerId, customerDTO);

            //Assert
            Assert.AreEqual(customerDTO.FirstName, result.FirstName);
            Assert.AreEqual(customerDTO.LastName, result.LastName);
            Assert.AreEqual(customerDTO.Email, result.Email);
            Assert.AreEqual(customerDTO.StreetName, result.StreetName);
            Assert.AreEqual(customerDTO.CityName, result.CityName);
            Assert.AreEqual(customerDTO.CountryName, result.CountryName);
        }

        [TestMethod]
        public void UpdateWithExistingEmailException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateWithExistingEmailException));
            var sut = new CustomerService(context);
            var customerActual = context.Customers.FirstOrDefault();
            var customerDTO = new CustomerDTO
            {
                Email = "pnikolov@abv.com"
            };


            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(customerActual.CustomerId, customerDTO));
        }

        [TestMethod]
        public void UpdateWithNullInputException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateWithNullInputException));
            var sut = new CustomerService(context);
            var customerActual = context.Customers.FirstOrDefault();

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(customerActual.CustomerId, null));
        }

        [TestMethod]
        public void UpdateAlreadyDeletedException()
        {
            //Arrange 
            var context = Utils.GetContext(nameof(UpdateAlreadyDeletedException));
            var sut = new CustomerService(context);
            var customerActual = context.Customers.FirstOrDefault();
            customerActual.IsDeleted = true;
            var customerDTO = new CustomerDTO
            {
                FirstName = "Johny",
                LastName = "Bravo",
                Email = "jbravo@ctn.com",
                StreetName = "12 West Road",
                CityName = "Los Angeles",
                CountryName = "USA"
            };

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Update(customerActual.CustomerId, customerDTO));
        }
    }
}
