﻿using DeliverIT.Database;
using DeliverIT.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DeliverIT.Tests
{
    public class Utils
    {
        public static DeliverITDbContext GetContext(string databaseName)
        {
            var options = new DbContextOptionsBuilder<DeliverITDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;


            var context = new DeliverITDbContext(options);

            context.Roles.AddRange(new List<Role>
            {
                new Role
                {
                    RoleId = 1,
                    Name = "Admin"
                },
                new Role
                {
                    RoleId = 2,
                    Name = "Employee"
                },
                new Role
                {
                    RoleId = 3,
                    Name = "Customer"
                }

            });

            context.Countries.AddRange(new List<Country>
            {
                new Country
                {
                    CountryId = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    CountryId = 2,
                    Name = "USA",
                },
                new Country
                {
                    CountryId = 3,
                    Name = "India",
                },
                new Country
                {
                    CountryId = 4,
                    Name = "Italy",
                }
            });

            context.Cities.AddRange(new List<City>()
            {
                new City
                {
                    CityId = 1,
                    Name = "Los Angeles",
                    CountryId = 2,
                },
                new City
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 3,
                    Name = "Plovdiv",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 4,
                    Name = "Gabrovo",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 5,
                    Name = "Trento",
                    CountryId = 4,
                }
            });

            context.Addresses.AddRange(new List<Address>()
            {
                new Address
                {
                    AddressId = 1,
                    CityId = 2,
                    StreetName = "Luben Karavelov 1",
                },
                new Address
                {
                    AddressId = 2,
                    CityId = 1,
                    StreetName = "12 North West Lane",
                },
                new Address
                {
                    AddressId = 3,
                    CityId = 3,
                    StreetName = "12 North West Lane",
                },
                new Address
                {
                    AddressId = 4,
                    CityId = 4,
                    StreetName = "Ivan Vazov 32",
                },
                new Address
                {
                    AddressId = 5,
                    CityId = 4,
                    StreetName = "Ivan Vazov 93",
                },
                new Address
                {
                    AddressId = 6,
                    CityId = 2,
                    StreetName = "Primorksa 4",
                },
                new Address
                {
                    AddressId = 7,
                    CityId = 2,
                    StreetName = "Via della Cervara, 104-108",
                },
                new Address
                {
                    AddressId = 8,
                    CityId = 2,
                    StreetName = "Via Alcide Degasperi, 45-37",
                },
                new Address
                {
                    AddressId = 9,
                    CityId = 2,
                    StreetName = "Delfinarium 3",
                },
            });

            context.Customers.AddRange(new List<Customer>()
            {
                new Customer
                {
                    CustomerId = 1,
                    FirstName = "Gonzo",
                    LastName = "ivanov",
                    Email = "ggg@gmail.com",
                    AddressForDeliveryId = 1
                },
                new Customer
                {
                    CustomerId = 2,
                    FirstName = "Petar",
                    LastName = "Nikolov",
                    Email = "pnikolov@abv.com",
                    AddressForDeliveryId = 4
                },
                new Customer
                {
                    CustomerId = 3,
                    FirstName = "Pesho",
                    LastName = "Stoqnov",
                    Email = "pstoqnov@yahoo.com",
                    AddressForDeliveryId = 5
                },
                new Customer
                {
                    CustomerId = 4,
                    FirstName = "Ivan",
                    LastName = "Monov",
                    Email = "vmonov@telerik.com",
                    AddressForDeliveryId = 6
                },
                new Customer
                {
                    CustomerId = 5,
                    FirstName = "Matey",
                    LastName = "Kaziiski",
                    Email = "mkaziiski@trento.com",
                    AddressForDeliveryId = 7
                }
            });

            context.Employees.AddRange(new List<Employee>
            {
                new Employee
                {
                    EmployeeId = 1,
                    FirstName = "Petar",
                    LastName = "Ivanov",
                    Email = "pivanov@gmail.com",
                    AddressId = 2
                },
                new Employee
                {
                    EmployeeId = 2,
                    FirstName = "Mincho",
                    LastName = "Praznikov",
                    Email = "mpraznikov@vremeto.bg",
                    AddressId = 9
                }

            });

            context.Warehouses.AddRange(new List<Warehouse>()
            {
                new Warehouse()
                {
                    WarehouseId = 1,
                    Name = "Kometa 3",
                    AddressId = 3
                },
                new Warehouse()
                {
                    WarehouseId = 2,
                    Name = "Trento WareHouse",
                    AddressId = 8
                }

            });

            context.Categories.AddRange(new List<Category>()
            {
                new Category
                {
                    CategoryId = 1,
                    Name = "Electronic"
                },
                new Category
                {
                    CategoryId = 2,
                    Name = "Clothing"
                },
                new Category
                {
                    CategoryId = 3,
                    Name = "Medical"
                },

            });

            context.Statuses.AddRange(new List<Status>()
            {
                new Status
                {
                    StatusId = 1,
                    Name = "Preparing"
                },
                new Status
                {
                    StatusId = 2,
                    Name = "On the way"
                },
                new Status
                {
                    StatusId = 3,
                    Name = "Completed"
                },

            });

            context.Shipments.AddRange(new List<Shipment>()
            {
                new Shipment()
                {
                    ShipmentId = 1,
                    Identificator = Guid.NewGuid().ToString(),
                    ArrivalDate = null,
                    DepartureDate = null,
                    StatusId = 1
                },
                new Shipment()
                {
                    ShipmentId = 2,
                    Identificator = Guid.NewGuid().ToString(),
                    ArrivalDate = null,
                    DepartureDate = DateTime.Now,
                    StatusId = 2
                },
                new Shipment()
                {
                    ShipmentId = 3,
                    Identificator = Guid.NewGuid().ToString(),
                    ArrivalDate = null,
                    DepartureDate = null,
                    StatusId = 1
                } 
            });

            context.Parcels.AddRange(new List<Parcel>()
            {
                new Parcel
                {
                    Weight = 32.2,
                    ParcelTrackNumber = Guid.NewGuid().ToString(),
                    ParcelId = 1,
                    CustomerId = 1,
                    CategoryId = 1,
                    EmployeeId = 1,
                    ShipmentId = 1,
                    WarehouseId = 1,
                },
                new Parcel
                {
                    Weight = 21.2,
                    ParcelTrackNumber = Guid.NewGuid().ToString(),
                    ParcelId = 2,
                    CustomerId = 5,
                    CategoryId = 2,
                    EmployeeId = 1,
                    ShipmentId = 1,
                    WarehouseId = 1,
                },
                new Parcel
                {
                    Weight = 100.32,
                    ParcelTrackNumber = Guid.NewGuid().ToString(),
                    ParcelId = 3,
                    CustomerId = 1,
                    CategoryId = 3,
                    EmployeeId = 1,
                    ShipmentId = 2,
                    WarehouseId = 2,
                },
                new Parcel
                {
                    Weight = 23,
                    ParcelTrackNumber = Guid.NewGuid().ToString(),
                    ParcelId = 4,
                    CustomerId = 1,
                    CategoryId = 3,
                    EmployeeId = 1,
                    ShipmentId = 2,
                    WarehouseId = 2,
                },
                new Parcel
                {
                    Weight = 54.12,
                    ParcelTrackNumber = Guid.NewGuid().ToString(),
                    ParcelId = 5,
                    CustomerId = 1,
                    CategoryId = 2,
                    EmployeeId = 1,
                    ShipmentId = 3,
                    WarehouseId = 2,
                }
            });

            context.EmployeeRoles.AddRange(new List<EmployeeRole>
            {
                new EmployeeRole()
                {
                    EmployeeRoleId = 1,
                    EmployeeId = 1,
                    RoleId = 1,
                },
                new EmployeeRole()
                {
                    EmployeeRoleId = 2,
                    EmployeeId = 1,
                    RoleId = 2,
                },
                new EmployeeRole()
                {
                    EmployeeRoleId = 3,
                    EmployeeId = 2,
                    RoleId = 2,
                },
            });



            context.SaveChanges();

            return context;
        }

        public static DbContextOptions<DeliverITDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<DeliverITDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
