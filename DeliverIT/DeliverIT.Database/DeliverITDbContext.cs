﻿using DeliverIT.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeliverIT.Database
{
    public class DeliverITDbContext : DbContext
    {
        public DeliverITDbContext(DbContextOptions<DeliverITDbContext> options)
            : base(options)
        {

        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Parcel> Parcels { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<CustomerRole> CustomerRoles { get; set; }
        public DbSet<EmployeeRole> EmployeeRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            // Categories
            builder.Entity<Category>()
                 .Property(c => c.Name)
                 .IsRequired();

            // Cities
            builder.Entity<City>()
                .Property(c => c.Name)
                .HasMaxLength(30)
                .IsRequired();

            // Countries
            builder.Entity<Country>()
                .Property(c => c.Name)
                .HasMaxLength(30)
                .IsRequired();

            // Customers
            builder.Entity<Customer>()
                .Property(c => c.FirstName)
                .HasMaxLength(30)
                .IsRequired();

            builder.Entity<Customer>()
                .Property(c => c.LastName)
                .HasMaxLength(30)
                .IsRequired();

            builder.Entity<Customer>()
                .Property(c => c.Email)
                .HasMaxLength(30)
                .IsRequired();

            builder.Entity<Customer>()
                .Property(c => c.IsDeleted)
                .HasDefaultValue(false);

            // Employees
            builder.Entity<Employee>()
                .Property(e => e.FirstName)
                .IsRequired();

            builder.Entity<Employee>()
                .Property(e => e.LastName)
                .IsRequired();

            builder.Entity<Employee>()
                 .Property(e => e.Email)
                 .IsRequired();

            // Statuses
            builder.Entity<Status>()
                .Property(s => s.Name)
                .IsRequired();

            // Warehouses
            builder.Entity<Warehouse>()
                .Property(w => w.Name)
                .IsRequired();

            // Addresses
            builder.Entity<Address>()
                .HasKey(a => a.AddressId);

            builder.Entity<Address>()
                .Property(a => a.StreetName)
                .IsRequired();

            // Configure 1-to-many between Address and City
            builder.Entity<Address>()
                .HasOne(a => a.City)
                .WithMany(c => c.Addresses)
                .HasForeignKey(a => a.CityId)
                .OnDelete(DeleteBehavior.Restrict);

            // Parcels
            builder.Entity<Parcel>()
                .Property(s => s.ParcelTrackNumber)
                .IsRequired();

            builder.Entity<Parcel>()
                .Property(w => w.ShipmentId)
                .IsRequired(false);

            builder.Entity<Parcel>()
                .Property(p => p.ShipmentId)
                .IsRequired(false);

            // Configure 1-to-many between Parcel and Category
            builder.Entity<Parcel>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Parcels)
                .HasForeignKey(p => p.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            // Configure 1-to-many between Parcel and Customer
            builder.Entity<Parcel>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.Parcels)
                .HasForeignKey(a => a.CustomerId)
                .OnDelete(DeleteBehavior.Restrict);

            // Configure 1-to-many between Parcel and Employee
            builder.Entity<Parcel>()
                .HasOne(p => p.Employee)
                .WithMany(e => e.Parcels)
                .HasForeignKey(p => p.EmployeeId)
                .OnDelete(DeleteBehavior.Restrict);

            // Configure 1-to-many between Parcel and Shipment
            builder.Entity<Parcel>()
                .HasOne(p => p.Shipment)
                .WithMany(s => s.Parcels)
                .HasForeignKey(p => p.ShipmentId)
                .OnDelete(DeleteBehavior.Restrict);

            // Configure 1-to-many between Parcel and Warehouse
            builder.Entity<Parcel>()
                 .HasOne(p => p.Warehouse)
                 .WithMany(s => s.Parcels)
                 .HasForeignKey(p => p.WarehouseId)
                 .OnDelete(DeleteBehavior.Restrict);

            // Shipment
            builder.Entity<Shipment>()
                .Property(s => s.DepartureDate)
                .IsRequired(false);

            builder.Entity<Shipment>()
                .Property(s => s.ArrivalDate)
                .IsRequired(false);

            // CustomerRole
            builder
                .Entity<CustomerRole>()
                .HasKey(cr => cr.CustomerRoleId);

            // Configure many-to-many between Role and Customer using CustomerRole
            builder.Entity<CustomerRole>()
                .HasOne(cr => cr.Role)
                .WithMany(r => r.Customer)
                .HasForeignKey(cr => cr.CustomerRoleId);

            builder.Entity<CustomerRole>()
                .HasOne(cr => cr.Role)
                .WithMany(r => r.Customer)
                .HasForeignKey(cr => cr.RoleId);

            // EmployeeRole
            builder
                .Entity<EmployeeRole>()
                .HasKey(e => e.EmployeeRoleId);

            // Configure many-to-many between Role and Employee using EmployeeRole
            builder.Entity<EmployeeRole>()
                .HasOne(er => er.Employee)
                .WithMany(r => r.Roles)
                .HasForeignKey(er => er.EmployeeId);

            builder.Entity<EmployeeRole>()
                .HasOne(er => er.Role)
                .WithMany(r => r.Employee)
                .HasForeignKey(er => er.RoleId);


            SeedData(builder);


            base.OnModelCreating(builder);
        }

        
        private static void SeedData(ModelBuilder builder)
        {
            var roles = new List<Role>
            {
                new Role
                {
                    RoleId = 1,
                    Name = "Admin"
                },
                new Role
                {
                    RoleId = 2,
                    Name = "Employee"
                },
                new Role
                {
                    RoleId = 3,
                    Name = "Customer"
                }

            };

            var countries = new List<Country>
            {
                new Country
                {
                    CountryId = 1,
                    Name = "Bulgaria",
                },
                new Country
                {
                    CountryId = 2,
                    Name = "USA",
                },
                new Country
                {
                    CountryId = 3,
                    Name = "India",
                },
                new Country
                {
                    CountryId = 4,
                    Name = "Italy",
                }
            };

            var cities = new List<City>()
            {
                new City
                {
                    CityId = 1,
                    Name = "Los Angeles",
                    CountryId = 2,
                },
                new City
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 3,
                    Name = "Plovdiv",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 4,
                    Name = "Gabrovo",
                    CountryId = 1,
                },
                new City
                {
                    CityId = 5,
                    Name = "Trento",
                    CountryId = 4,
                }
            };

            var addresses = new List<Address>()
            {
                new Address
                {
                    AddressId = 1,
                    CityId = 2,
                    StreetName = "Luben Karavelov 1",
                },
                new Address
                {
                    AddressId = 2,
                    CityId = 1,
                    StreetName = "12 North West Lane",
                },
                new Address
                {
                    AddressId = 3,
                    CityId = 3,
                    StreetName = "12 North West Lane",
                },
                new Address
                {
                    AddressId = 4,
                    CityId = 4,
                    StreetName = "Ivan Vazov 32",
                },
                new Address
                {
                    AddressId = 5,
                    CityId = 4,
                    StreetName = "Ivan Vazov 93",
                },
                new Address
                {
                    AddressId = 6,
                    CityId = 2,
                    StreetName = "Primorksa 4",
                },
                new Address
                {
                    AddressId = 7,
                    CityId = 2,
                    StreetName = "Via della Cervara, 104-108",
                },
                new Address
                {
                    AddressId = 8,
                    CityId = 2,
                    StreetName = "Via Alcide Degasperi, 45-37",
                },
                new Address
                {
                    AddressId = 9,
                    CityId = 2,
                    StreetName = "Delfinarium 3",
                },
            };

            var customers = new List<Customer>()
            {
                new Customer
                {
                    CustomerId = 1,
                    FirstName = "Gonzo",
                    LastName = "ivanov",
                    Email = "ggg@gmail.com",
                    AddressForDeliveryId = 1
                },
                new Customer
                {
                    CustomerId = 2,
                    FirstName = "Petar",
                    LastName = "Nikolov",
                    Email = "pnikolov@abv.com",
                    AddressForDeliveryId = 4
                },
                new Customer
                {
                    CustomerId = 3,
                    FirstName = "Pesho",
                    LastName = "Stoqnov",
                    Email = "pstoqnov@yahoo.com",
                    AddressForDeliveryId = 5
                },
                new Customer
                {
                    CustomerId = 4,
                    FirstName = "Ivan",
                    LastName = "Monov",
                    Email = "vmonov@telerik.com",
                    AddressForDeliveryId = 6
                },
                new Customer
                {
                    CustomerId = 5,
                    FirstName = "Matey",
                    LastName = "Kaziiski",
                    Email = "mkaziiski@trento.com",
                    AddressForDeliveryId = 7
                }
            };

            var employees = new List<Employee>
            {
                new Employee
                {
                    EmployeeId = 1,
                    FirstName = "Petar",
                    LastName = "Ivanov",
                    Email = "pivanov@gmail.com",
                    AddressId = 2
                },
                new Employee
                {
                    EmployeeId = 2,
                    FirstName = "Mincho",
                    LastName = "Praznikov",
                    Email = "mpraznikov@vremeto.bg",
                    AddressId = 9
                }

            };

            var employeeRoles = new List<EmployeeRole>
            {
                new EmployeeRole()
                {
                    EmployeeRoleId = 1,
                    EmployeeId = 1,
                    RoleId = 1,
                },
                new EmployeeRole()
                {
                    EmployeeRoleId = 2,
                    EmployeeId = 1,
                    RoleId = 2,
                },
                new EmployeeRole()
                {
                    EmployeeRoleId = 3,
                    EmployeeId = 2,
                    RoleId = 2,
                },
            };

            var warehouses = new List<Warehouse>()
            {
                new Warehouse()
                {
                    WarehouseId = 1,
                    Name = "Kometa 3",
                    AddressId = 3
                },
                new Warehouse()
                {
                    WarehouseId = 2,
                    Name = "Trento WareHouse",
                    AddressId = 8
                }
                
            };

            var categories = new List<Category>()
            {
                new Category
                {
                    CategoryId = 1,
                    Name = "Electronic"
                },
                new Category
                {
                    CategoryId = 2,
                    Name = "Clothing"
                },
                new Category
                {
                    CategoryId = 3,
                    Name = "Medical"
                },

            };

            var statuses = new List<Status>()
            {   
                new Status
                {
                    StatusId = 1,
                    Name = "Preparing"
                },
                new Status
                {
                    StatusId = 2,
                    Name = "On the way"
                },
                new Status
                {
                    StatusId = 3,
                    Name = "Completed"
                },

            };

            
            var shipments = new List<Shipment>()
            {
                new Shipment()
                {
                    ShipmentId = 1,
                    Identificator = "6q2nyzj1kw7A021O",
                    ArrivalDate = null,
                    DepartureDate = null,
                    StatusId = 1
                },
                new Shipment()
                {
                    ShipmentId = 2,
                    Identificator = "3WD9xXsap0cmh6ZC",
                    ArrivalDate = null,
                    DepartureDate = DateTime.Now,
                    StatusId = 2
                },
                new Shipment()
                {
                    ShipmentId = 3,
                    Identificator = "cFLvluOq751rdGCX",
                    ArrivalDate = null,
                    DepartureDate = null,
                    StatusId = 1
                },
            };

            var shipment2 = new Shipment()
            {
                ShipmentId = 2,
                Identificator = "asd1",
                ArrivalDate = new DateTime(2031, 5, 6),
                DepartureDate = new DateTime(2031, 5, 10),
                StatusId = 2
            };

            var parcels = new List<Parcel>()
            {
                new Parcel
                {
                    Weight = 32.2,
                    ParcelTrackNumber = "ASkL1QLDYtHl0io4",
                    ParcelId = 1,
                    CustomerId = 1,
                    CategoryId = 1,
                    EmployeeId = 1,
                    ShipmentId = 1,
                    WarehouseId = 1,
                },
                new Parcel
                {
                    Weight = 21.2,
                    ParcelTrackNumber = "w1raSr7BJJMh2GiD",
                    ParcelId = 2,
                    CustomerId = 5,
                    CategoryId = 2,
                    EmployeeId = 1,
                    ShipmentId = 1,
                    WarehouseId = 1,
                },
                new Parcel
                {
                    Weight = 100.32,
                    ParcelTrackNumber = "WixTu31gAwMZJE5C",
                    ParcelId = 3,
                    CustomerId = 1,
                    CategoryId = 3,
                    EmployeeId = 1,
                    ShipmentId = 2,
                    WarehouseId = 2,
                },
                new Parcel
                {
                    Weight = 23,
                    ParcelTrackNumber = "1NUiQViR6g8CLk8F",
                    ParcelId = 4,
                    CustomerId = 1,
                    CategoryId = 3,
                    EmployeeId = 1,
                    ShipmentId = 2,
                    WarehouseId = 2,
                },
                new Parcel
                {
                    Weight = 54.12,
                    ParcelTrackNumber = "wHBxaGu2gUYgXlky",
                    ParcelId = 5,
                    CustomerId = 1,
                    CategoryId = 2,
                    EmployeeId = 1,
                    ShipmentId = 3,
                    WarehouseId = 2,
                }
            };

            builder.Entity<Role>().HasData(roles);
            builder.Entity<Status>().HasData(statuses);
            builder.Entity<Category>().HasData(categories);

            builder.Entity<Country>().HasData(countries);
            builder.Entity<City>().HasData(cities);
            builder.Entity<Address>().HasData(addresses);
            builder.Entity<Customer>().HasData(customers);
            builder.Entity<Employee>().HasData(employees);
            builder.Entity<Warehouse>().HasData(warehouses);

            builder.Entity<EmployeeRole>().HasData(employeeRoles);
            builder.Entity<Parcel>().HasData(parcels);
            builder.Entity<Shipment>().HasData(shipments);
            
            
        }
    }
}
