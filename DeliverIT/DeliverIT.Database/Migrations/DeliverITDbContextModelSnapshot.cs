﻿// <auto-generated />
using System;
using DeliverIT.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DeliverIT.Database.Migrations
{
    [DbContext(typeof(DeliverITDbContext))]
    partial class DeliverITDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DeliverIT.Models.Address", b =>
                {
                    b.Property<int>("AddressId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CityId")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<string>("StreetName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("AddressId");

                    b.HasIndex("CityId");

                    b.ToTable("Addresses");

                    b.HasData(
                        new
                        {
                            AddressId = 1,
                            CityId = 2,
                            IsDeleted = false,
                            StreetName = "Luben Karavelov 1"
                        },
                        new
                        {
                            AddressId = 2,
                            CityId = 1,
                            IsDeleted = false,
                            StreetName = "12 North West Lane"
                        },
                        new
                        {
                            AddressId = 3,
                            CityId = 3,
                            IsDeleted = false,
                            StreetName = "12 North West Lane"
                        },
                        new
                        {
                            AddressId = 4,
                            CityId = 4,
                            IsDeleted = false,
                            StreetName = "Ivan Vazov 32"
                        },
                        new
                        {
                            AddressId = 5,
                            CityId = 4,
                            IsDeleted = false,
                            StreetName = "Ivan Vazov 93"
                        },
                        new
                        {
                            AddressId = 6,
                            CityId = 2,
                            IsDeleted = false,
                            StreetName = "Primorksa 4"
                        },
                        new
                        {
                            AddressId = 7,
                            CityId = 2,
                            IsDeleted = false,
                            StreetName = "Via della Cervara, 104-108"
                        },
                        new
                        {
                            AddressId = 8,
                            CityId = 2,
                            IsDeleted = false,
                            StreetName = "Via Alcide Degasperi, 45-37"
                        },
                        new
                        {
                            AddressId = 9,
                            CityId = 2,
                            IsDeleted = false,
                            StreetName = "Delfinarium 3"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Category", b =>
                {
                    b.Property<int>("CategoryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CategoryId");

                    b.ToTable("Categories");

                    b.HasData(
                        new
                        {
                            CategoryId = 1,
                            Name = "Electronic"
                        },
                        new
                        {
                            CategoryId = 2,
                            Name = "Clothing"
                        },
                        new
                        {
                            CategoryId = 3,
                            Name = "Medical"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.City", b =>
                {
                    b.Property<int>("CityId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CountryId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.HasKey("CityId");

                    b.HasIndex("CountryId");

                    b.ToTable("Cities");

                    b.HasData(
                        new
                        {
                            CityId = 1,
                            CountryId = 2,
                            Name = "Los Angeles"
                        },
                        new
                        {
                            CityId = 2,
                            CountryId = 1,
                            Name = "Varna"
                        },
                        new
                        {
                            CityId = 3,
                            CountryId = 1,
                            Name = "Plovdiv"
                        },
                        new
                        {
                            CityId = 4,
                            CountryId = 1,
                            Name = "Gabrovo"
                        },
                        new
                        {
                            CityId = 5,
                            CountryId = 4,
                            Name = "Trento"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Country", b =>
                {
                    b.Property<int>("CountryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.HasKey("CountryId");

                    b.ToTable("Countries");

                    b.HasData(
                        new
                        {
                            CountryId = 1,
                            Name = "Bulgaria"
                        },
                        new
                        {
                            CountryId = 2,
                            Name = "USA"
                        },
                        new
                        {
                            CountryId = 3,
                            Name = "India"
                        },
                        new
                        {
                            CountryId = 4,
                            Name = "Italy"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Customer", b =>
                {
                    b.Property<int>("CustomerId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AddressForDeliveryId")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<bool>("IsDeleted")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasDefaultValue(false);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.HasKey("CustomerId");

                    b.HasIndex("AddressForDeliveryId")
                        .IsUnique();

                    b.ToTable("Customers");

                    b.HasData(
                        new
                        {
                            CustomerId = 1,
                            AddressForDeliveryId = 1,
                            Email = "ggg@gmail.com",
                            FirstName = "Gonzo",
                            IsDeleted = false,
                            LastName = "ivanov"
                        },
                        new
                        {
                            CustomerId = 2,
                            AddressForDeliveryId = 4,
                            Email = "pnikolov@abv.com",
                            FirstName = "Petar",
                            IsDeleted = false,
                            LastName = "Nikolov"
                        },
                        new
                        {
                            CustomerId = 3,
                            AddressForDeliveryId = 5,
                            Email = "pstoqnov@yahoo.com",
                            FirstName = "Pesho",
                            IsDeleted = false,
                            LastName = "Stoqnov"
                        },
                        new
                        {
                            CustomerId = 4,
                            AddressForDeliveryId = 6,
                            Email = "vmonov@telerik.com",
                            FirstName = "Ivan",
                            IsDeleted = false,
                            LastName = "Monov"
                        },
                        new
                        {
                            CustomerId = 5,
                            AddressForDeliveryId = 7,
                            Email = "mkaziiski@trento.com",
                            FirstName = "Matey",
                            IsDeleted = false,
                            LastName = "Kaziiski"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.CustomerRole", b =>
                {
                    b.Property<int>("CustomerRoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CustomerId")
                        .HasColumnType("int");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("CustomerRoleId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("RoleId");

                    b.ToTable("CustomerRoles");
                });

            modelBuilder.Entity("DeliverIT.Models.Employee", b =>
                {
                    b.Property<int>("EmployeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AddressId")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("EmployeeId");

                    b.HasIndex("AddressId")
                        .IsUnique();

                    b.ToTable("Employees");

                    b.HasData(
                        new
                        {
                            EmployeeId = 1,
                            AddressId = 2,
                            Email = "pivanov@gmail.com",
                            FirstName = "Petar",
                            IsDeleted = false,
                            LastName = "Ivanov"
                        },
                        new
                        {
                            EmployeeId = 2,
                            AddressId = 9,
                            Email = "mpraznikov@vremeto.bg",
                            FirstName = "Mincho",
                            IsDeleted = false,
                            LastName = "Praznikov"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.EmployeeRole", b =>
                {
                    b.Property<int>("EmployeeRoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("EmployeeRoleId");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("RoleId");

                    b.ToTable("EmployeeRoles");

                    b.HasData(
                        new
                        {
                            EmployeeRoleId = 1,
                            EmployeeId = 1,
                            RoleId = 1
                        },
                        new
                        {
                            EmployeeRoleId = 2,
                            EmployeeId = 1,
                            RoleId = 2
                        },
                        new
                        {
                            EmployeeRoleId = 3,
                            EmployeeId = 2,
                            RoleId = 2
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Parcel", b =>
                {
                    b.Property<int>("ParcelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CategoryId")
                        .HasColumnType("int");

                    b.Property<int>("CustomerId")
                        .HasColumnType("int");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<string>("ParcelTrackNumber")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("ShipmentId")
                        .HasColumnType("int");

                    b.Property<int>("WarehouseId")
                        .HasColumnType("int");

                    b.Property<double>("Weight")
                        .HasColumnType("float");

                    b.HasKey("ParcelId");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("ShipmentId");

                    b.HasIndex("WarehouseId");

                    b.ToTable("Parcels");

                    b.HasData(
                        new
                        {
                            ParcelId = 1,
                            CategoryId = 1,
                            CustomerId = 1,
                            EmployeeId = 1,
                            IsDeleted = false,
                            ParcelTrackNumber = "ASkL1QLDYtHl0io4",
                            ShipmentId = 1,
                            WarehouseId = 1,
                            Weight = 32.200000000000003
                        },
                        new
                        {
                            ParcelId = 2,
                            CategoryId = 2,
                            CustomerId = 5,
                            EmployeeId = 1,
                            IsDeleted = false,
                            ParcelTrackNumber = "w1raSr7BJJMh2GiD",
                            ShipmentId = 1,
                            WarehouseId = 1,
                            Weight = 21.199999999999999
                        },
                        new
                        {
                            ParcelId = 3,
                            CategoryId = 3,
                            CustomerId = 1,
                            EmployeeId = 1,
                            IsDeleted = false,
                            ParcelTrackNumber = "WixTu31gAwMZJE5C",
                            ShipmentId = 2,
                            WarehouseId = 2,
                            Weight = 100.31999999999999
                        },
                        new
                        {
                            ParcelId = 4,
                            CategoryId = 3,
                            CustomerId = 1,
                            EmployeeId = 1,
                            IsDeleted = false,
                            ParcelTrackNumber = "1NUiQViR6g8CLk8F",
                            ShipmentId = 2,
                            WarehouseId = 2,
                            Weight = 23.0
                        },
                        new
                        {
                            ParcelId = 5,
                            CategoryId = 2,
                            CustomerId = 1,
                            EmployeeId = 1,
                            IsDeleted = false,
                            ParcelTrackNumber = "wHBxaGu2gUYgXlky",
                            ShipmentId = 3,
                            WarehouseId = 2,
                            Weight = 54.119999999999997
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Role", b =>
                {
                    b.Property<int>("RoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("RoleId");

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            RoleId = 1,
                            Name = "Admin"
                        },
                        new
                        {
                            RoleId = 2,
                            Name = "Employee"
                        },
                        new
                        {
                            RoleId = 3,
                            Name = "Customer"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Shipment", b =>
                {
                    b.Property<int>("ShipmentId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("ArrivalDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("DepartureDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Identificator")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<int>("StatusId")
                        .HasColumnType("int");

                    b.HasKey("ShipmentId");

                    b.HasIndex("StatusId");

                    b.ToTable("Shipments");

                    b.HasData(
                        new
                        {
                            ShipmentId = 1,
                            Identificator = "6q2nyzj1kw7A021O",
                            IsDeleted = false,
                            StatusId = 1
                        },
                        new
                        {
                            ShipmentId = 2,
                            DepartureDate = new DateTime(2021, 4, 22, 10, 14, 7, 849, DateTimeKind.Local).AddTicks(6587),
                            Identificator = "3WD9xXsap0cmh6ZC",
                            IsDeleted = false,
                            StatusId = 2
                        },
                        new
                        {
                            ShipmentId = 3,
                            Identificator = "cFLvluOq751rdGCX",
                            IsDeleted = false,
                            StatusId = 1
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Status", b =>
                {
                    b.Property<int>("StatusId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("StatusId");

                    b.ToTable("Statuses");

                    b.HasData(
                        new
                        {
                            StatusId = 1,
                            Name = "Preparing"
                        },
                        new
                        {
                            StatusId = 2,
                            Name = "On the way"
                        },
                        new
                        {
                            StatusId = 3,
                            Name = "Completed"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Warehouse", b =>
                {
                    b.Property<int>("WarehouseId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AddressId")
                        .HasColumnType("int");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("WarehouseId");

                    b.HasIndex("AddressId")
                        .IsUnique();

                    b.ToTable("Warehouses");

                    b.HasData(
                        new
                        {
                            WarehouseId = 1,
                            AddressId = 3,
                            IsDeleted = false,
                            Name = "Kometa 3"
                        },
                        new
                        {
                            WarehouseId = 2,
                            AddressId = 8,
                            IsDeleted = false,
                            Name = "Trento WareHouse"
                        });
                });

            modelBuilder.Entity("DeliverIT.Models.Address", b =>
                {
                    b.HasOne("DeliverIT.Models.City", "City")
                        .WithMany("Addresses")
                        .HasForeignKey("CityId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.City", b =>
                {
                    b.HasOne("DeliverIT.Models.Country", "Country")
                        .WithMany("Cities")
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.Customer", b =>
                {
                    b.HasOne("DeliverIT.Models.Address", "AddressForDelivery")
                        .WithOne("Customer")
                        .HasForeignKey("DeliverIT.Models.Customer", "AddressForDeliveryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.CustomerRole", b =>
                {
                    b.HasOne("DeliverIT.Models.Customer", "Customer")
                        .WithMany("Roles")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DeliverIT.Models.Role", "Role")
                        .WithMany("Customer")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.Employee", b =>
                {
                    b.HasOne("DeliverIT.Models.Address", "Address")
                        .WithOne("Employee")
                        .HasForeignKey("DeliverIT.Models.Employee", "AddressId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.EmployeeRole", b =>
                {
                    b.HasOne("DeliverIT.Models.Employee", "Employee")
                        .WithMany("Roles")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DeliverIT.Models.Role", "Role")
                        .WithMany("Employee")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.Parcel", b =>
                {
                    b.HasOne("DeliverIT.Models.Category", "Category")
                        .WithMany("Parcels")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("DeliverIT.Models.Customer", "Customer")
                        .WithMany("Parcels")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("DeliverIT.Models.Employee", "Employee")
                        .WithMany("Parcels")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("DeliverIT.Models.Shipment", "Shipment")
                        .WithMany("Parcels")
                        .HasForeignKey("ShipmentId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DeliverIT.Models.Warehouse", "Warehouse")
                        .WithMany("Parcels")
                        .HasForeignKey("WarehouseId")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.Shipment", b =>
                {
                    b.HasOne("DeliverIT.Models.Status", "Status")
                        .WithMany("Shipments")
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DeliverIT.Models.Warehouse", b =>
                {
                    b.HasOne("DeliverIT.Models.Address", "Address")
                        .WithOne("Warehouse")
                        .HasForeignKey("DeliverIT.Models.Warehouse", "AddressId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
