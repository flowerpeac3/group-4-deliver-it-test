﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }

        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>();
    }
}
