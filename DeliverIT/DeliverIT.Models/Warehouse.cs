﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Warehouse
    {
        public int WarehouseId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public int AddressId { get; set; }

        public Address Address { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>(); 
    }
}
