﻿namespace DeliverIT.Models
{
    public class Parcel
    {
        public int ParcelId { get; set; }
        public double Weight { get; set; }
        public bool IsDeleted { get; set; }
        public int CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public int WarehouseId { get; set; }
        public int CategoryId { get; set; }
        public int? ShipmentId { get; set; }

        public Shipment Shipment { get; set; }
        public string ParcelTrackNumber { get; set; }
        public Customer Customer { get; set; }
        public Employee Employee { get; set; }
        public Warehouse Warehouse { get; set; }
        public Category Category { get; set; }
    }
}
