﻿using System;
using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Shipment
    {
        public int ShipmentId { get; set; }
        public string Identificator { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public bool IsDeleted { get; set; }
        public int StatusId { get; set; }
        
        public Status Status { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>();
    }
}
