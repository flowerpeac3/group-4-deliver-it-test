﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Country
    {
        public int CountryId { get; set; }
        public string Name { get; set; }

        public List<City> Cities { get; set; } = new List<City>();
    }
}
