﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        public string Name { get; set; }

        public ICollection<CustomerRole> Customer { get; set; } = new List<CustomerRole>();
        public ICollection<EmployeeRole> Employee { get; set; } = new List<EmployeeRole>();
    }
}
