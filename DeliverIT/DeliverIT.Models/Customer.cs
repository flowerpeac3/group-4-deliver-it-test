﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
        public int AddressForDeliveryId { get; set; }

        public Address AddressForDelivery { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>();
        public ICollection<CustomerRole> Roles { get; set; } = new List<CustomerRole>();
    }
}
