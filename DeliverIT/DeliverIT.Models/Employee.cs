﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
        public int AddressId { get; set; }

        public Address Address { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>();
        public ICollection<EmployeeRole> Roles { get; set; } = new List<EmployeeRole>();

    }
}
