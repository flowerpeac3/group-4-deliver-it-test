﻿using System.Collections.Generic;

namespace DeliverIT.Models
{
    public class City
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public Country Country { get; set; }
        public ICollection<Address> Addresses { get; set; } = new List<Address>();
    }
}
