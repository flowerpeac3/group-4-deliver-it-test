﻿namespace DeliverIT.Models
{
    public class CustomerRole
    {
        public int CustomerRoleId { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
