﻿namespace DeliverIT.Models
{
    public class Address
    {
        public int AddressId { get; set; }
        public string StreetName { get; set; }
        public bool IsDeleted { get; set; }
        public int CityId { get; set; }

        public City City { get; set; }
        public Warehouse Warehouse { get; set; }
        public Employee Employee { get; set; }
        public Customer Customer { get; set; }
    }
}
