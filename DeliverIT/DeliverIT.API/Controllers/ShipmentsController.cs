﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Database;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentsController : ControllerBase
    {
        private readonly IShipmentService shipmentService;
        private readonly IAuthHelper authHelper;

        public ShipmentsController(IShipmentService shipmentService, IAuthHelper authHelper)
        {
            this.shipmentService = shipmentService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.GetAll();

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.Get(id);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.Create();

                return Created("", result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPost("addparceltoshipment")]
        public IActionResult AddParcelToShipment([FromHeader] string credentials, [FromBody] ShipmentParcelDTO shipmentParcel)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.AddParcelToShipment(shipmentParcel);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPost("removeparcelshipment")]
        public IActionResult RemoveParcelFromShipment([FromHeader] string credentials, [FromBody] ShipmentParcelDTO shipmentParcel)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.RemoveParcelToShipment(shipmentParcel);
                
                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("searchstatus")]
        public IActionResult SearchByStatus([FromHeader] string credentials, [FromQuery] string value)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.GetByStatus(value);

                return Ok($"Shipments {value} " + result.Count());
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("getcustomerparcels")]
        public IActionResult GetCustomerParcels([FromHeader] string credentials, [FromQuery] int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    var result = this.shipmentService.GetCustomerParcels(id);
                    return Ok(result);
                }
                else
                {
                    if(customer != null)
                    {

                        var result = this.shipmentService.GetCustomerParcels(customer.CustomerId);
                        return Ok(result);
                    }
                    
                    return BadRequest("Invalid Authentication!");
                }      
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("status")]
        public IActionResult GetCustomerParcelStatus([FromHeader] string credentials, [FromQuery] int id, string value)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    var result = this.shipmentService.GetCustomerParcelStatus(id, value);
                    return Ok(result);
                }
                else
                {
                    if (customer != null)
                    {

                        var result = this.shipmentService.GetCustomerParcelStatus(customer.CustomerId, value);
                        return Ok(result);
                    }

                    return BadRequest("Invalid Authentication!");
                }

            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("warehouse")]
        public IActionResult GetWarehouseShipmentsStatuses([FromHeader] string credentials, [FromQuery] string name)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.GetWarehouseShipmentsStatuses(name);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("filter")]
        public IActionResult Get([FromHeader] string credentials, [FromQuery] string warehouse, string customeremail)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.Filter(warehouse, customeremail);

                return Ok(result);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] ShipmentDTO updateShipment)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.shipmentService.Update(id, updateShipment);

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                if (this.shipmentService.Delete(id))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
