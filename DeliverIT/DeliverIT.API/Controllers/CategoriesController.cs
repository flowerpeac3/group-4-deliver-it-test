﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        private readonly IAuthHelper authHelper;

        public CategoriesController(ICategoryService categoryService, IAuthHelper authHelper)
        {
            this.categoryService = categoryService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {

            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    return Ok(this.categoryService.GetAll());
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(this.categoryService.GetAll());
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }     
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);
                var category = this.categoryService.Get(id);

                if (employee != null)
                {
                    return Ok(category);
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(category);
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] Category newCategory)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.categoryService.Create(newCategory);

                return Created(("api/users/" + result.CategoryId), result);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] Category newCategory)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.categoryService.Update(id, newCategory.Name);
                return Ok(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                categoryService.Delete(id);

                return Ok();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
