﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParcelsController : ControllerBase
    {
        private readonly IParcelService parcelService;
        private readonly IAuthHelper authHelper;

        public ParcelsController(IParcelService parcelService, IAuthHelper authHelper)
        {
            this.parcelService = parcelService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.parcelService.GetAll();

                if (result.Count() == 0)
                {
                    return NoContent();
                }

                return Ok(result);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.parcelService.Get(id);

                return Ok(result);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] ParcelDTO newStatus)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.parcelService.Create(newStatus);
                return Created(("api/users/" + result.CustomerName), result);
            }
            catch (ArgumentNullException)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] ParcelDTO newParcel)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.parcelService.Update(id, newParcel);
                if (result == null)
                {
                    return BadRequest();
                }

                return Ok(result);
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                this.parcelService.Delete(id);

                return Ok();
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (DbUpdateException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("filter")]
        public IActionResult Filter([FromHeader] string credentials, [FromQuery] string filter, string value)
        {
            try
            {
                var customer = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (customer == null)
                    return BadRequest("Invalid Authentication!");

                var filterByCustomerFirstName = parcelService.FIlterByCustomerFirstName(value);
                var filterByCustomerLastName = parcelService.FIlterByCustomerLastName(value);
                var filterByWarehouse = parcelService.FIlterByWarehouse(value);
                var filterByCategory = parcelService.FIlterByCategory(value);
                //var filterByWeight = parcelService.FIlterByWeight(double.Parse(value));

                switch (filter.ToLower())
                {
                    case "customerfirstname":
                        return Ok(filterByCustomerFirstName);
                    case "customerlastname":
                        return Ok(filterByCustomerLastName);
                    case "warehousename":
                        return Ok(filterByWarehouse);
                    case "category":
                        return Ok(filterByCategory);


                    default: return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpGet("parcels")]

        public IActionResult AllParcels([FromHeader] string credentials, [FromQuery] string name, string status)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null && name != null)
                {
                    var pastParcels = this.parcelService.PastParcels(name);
                    var onTheWay = this.parcelService.ParcelsOnTheWay(name);

                    switch (status)
                    {
                        case "pastparcels":
                            return Ok(pastParcels);
                        case "ontheway":
                            return Ok(onTheWay);

                        default: return BadRequest();
                    }

                }
                else
                {
                    if (customer != null)
                    {

                        var pastParcels = this.parcelService.PastParcels(customer.FirstName);
                        var onTheWay = this.parcelService.ParcelsOnTheWay(customer.FirstName);

                        switch (status)
                        {
                            case "pastparcels":
                                return Ok(pastParcels);
                            case "ontheway":
                                return Ok(onTheWay);

                            default: return BadRequest();
                        }
                    }

                    return BadRequest("Invalid Authentication!");
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
