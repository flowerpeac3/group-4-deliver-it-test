﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class CitiesController : ControllerBase
    {
        private readonly ICityService cityService;
        private readonly IAuthHelper authHelper;

        public CitiesController(ICityService cityService, IAuthHelper authHelper)
        {
            this.cityService = cityService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    return Ok(this.cityService.GetAll());
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(this.cityService.GetAll());
                    }

                    return BadRequest("Invalid Authentication!");
                }

                
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    var city = this.cityService.Get(id);
                    return Ok(city);
                }
                else
                {
                    if (customer != null)
                    {
                        var city = this.cityService.Get(id);
                        return Ok(city);
                    }

                    return BadRequest("Invalid Authentication!");
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] City newCity)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                if (newCity == null)
                {
                    return BadRequest();
                }

                var result = this.cityService.Create(newCity);

                return Created(("api/users/" + result.CityId), result);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] City newCity)
        {
            try 
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.cityService.Update(id, newCity.Name, newCity.CountryId);
                return Ok(result);
            }
            catch(Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                cityService.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
