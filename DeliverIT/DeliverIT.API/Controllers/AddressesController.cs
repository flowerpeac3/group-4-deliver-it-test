﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AddressesController : ControllerBase
    {
        private readonly IAddressService addressService;
        private readonly IAuthHelper authHelper;

        public AddressesController(IAddressService addressService, IAuthHelper authHelper)
        {
            this.addressService = addressService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                return Ok(this.addressService.GetAll());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }


        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var category = this.addressService.Get(id);
                return Ok(category);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] Address newAddress)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                if (newAddress == null)
                {
                    return BadRequest();
                }

                var result = this.addressService.Create(newAddress);

                return Created(("api/users/" + result.AddressId), result);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] Address newAddress)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.addressService.Update(id, newAddress);
                return Ok(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                if (addressService.Delete(id))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }
    }
}
