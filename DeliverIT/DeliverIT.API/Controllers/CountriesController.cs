﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService countryService;
        private readonly IAuthHelper authHelper;

        public CountriesController(ICountryService countryService, IAuthHelper authHelper)
        {
            this.countryService = countryService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {

            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    return Ok(this.countryService.GetAll());
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(this.countryService.GetAll());
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);
                var result = this.countryService.Get(id);
               
                if (employee != null)
                {
                    return Ok(result);
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(result);
                    }

                    return BadRequest("Invalid Authentication!");
                }  
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] Country newCountry)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.countryService.Create(newCountry);

                return Created(("api/users/" + result.CountryId), result);

            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] Country newCountry)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.countryService.Update(id, newCountry.Name);
                return Ok(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                countryService.Delete(id);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }    
        }
    }
}
