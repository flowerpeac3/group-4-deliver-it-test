﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService customerService;
        private readonly IAuthHelper authHelper;

        public CustomersController(ICustomerService customerService, IAuthHelper authHelper)
        {
            this.customerService = customerService;
            this.authHelper = authHelper;
        }

        [HttpGet("getcustomerscount")]
        public IActionResult GetCustomersCount()
        {
            try
            {
                var result = this.customerService.CustomersCounter();

                return Ok(result);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("search")]
        public IActionResult Search([FromHeader] string credentials, [FromQuery] string email, string fname, string lname, string parcel)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");
                
                var result = this.customerService.Search(email, fname, lname, parcel);

                return Ok(result);

            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var result = this.customerService.GetAll();
                
                if (employee == null)
                    return BadRequest("Invalid Authentication!");
                    
                return Ok(result);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var result = this.customerService.Get(id);
                
                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                return Ok(result);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] CustomerDTO newCustomer)
        {
            try
            {
                var result = this.customerService.Create(newCustomer);
                
                return Created("", result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPut("")]
        public IActionResult Put([FromHeader] string credentials, [FromQuery] int id, [FromBody] CustomerDTO updateCustomer)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    var result = this.customerService.Update(id, updateCustomer);
                    return Ok(result);
                }
                else
                {
                    if (customer != null)
                    {

                        var result = this.customerService.Update(customer.CustomerId, updateCustomer);
                        return Ok(result);
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                this.customerService.Delete(id);
                return NoContent(); 
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
