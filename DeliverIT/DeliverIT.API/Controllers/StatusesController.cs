﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Database;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace DeliverIT.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatusesController : ControllerBase
    {
        private readonly IStatusService statusService;
        private readonly IAuthHelper authHelper;

        public StatusesController(IStatusService statusService, IAuthHelper authHelper)
        {
            this.statusService = statusService;
            this.authHelper = authHelper;
        }

        [HttpGet("")]
        public IActionResult GetAll([FromHeader] string credentials)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);

                if (employee != null)
                {
                    return Ok(this.statusService.GetAll());
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(this.statusService.GetAll());
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);
                var customer = this.authHelper.TryGetCustomerByEmail(credentials);
                var result = this.statusService.Get(id);

                if (employee != null)
                {
                    return Ok(result);
                }
                else
                {
                    if (customer != null)
                    {
                        return Ok(result);
                    }

                    return BadRequest("Invalid Authentication!");
                }
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        [HttpPost("")]
        public IActionResult Post([FromHeader] string credentials, [FromBody] Status newStatus)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.statusService.Create(newStatus);

                return Created(("api/users/" + result.StatusId), result);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }  
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string credentials, int id, [FromBody] Status newStatus)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                var result = this.statusService.Update(id, newStatus.Name);
                return Ok(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string credentials, int id)
        {
            try
            {
                var employee = this.authHelper.TryGetEmployeeByEmail(credentials);

                if (employee == null)
                    return BadRequest("Invalid Authentication!");

                statusService.Delete(id);
                return Ok();
                
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }
    }
}
