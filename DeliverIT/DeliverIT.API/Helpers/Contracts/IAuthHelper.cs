﻿using DeliverIT.Models;

namespace DeliverIT.API.Helpers.Contracts
{
    public interface IAuthHelper
    {
        Customer TryGetCustomerByEmail(string authorizationHeader);
        Employee TryGetEmployeeByEmail(string authorizationHeader);
    }

}
