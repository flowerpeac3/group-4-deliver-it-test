﻿using DeliverIT.API.Helpers.Contracts;
using DeliverIT.Models;
using DeliverIT.Services.Contracts;
using System;

namespace DeliverIT.API.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private ICustomerService customerService;
        private IEmployeeService employeeService;

        public AuthHelper(ICustomerService customerService, IEmployeeService employeeService)
        {
            this.customerService = customerService;
            this.employeeService = employeeService;
        }

        public Customer TryGetCustomerByEmail(string authorizationHeader)
        {
            return this.customerService.GetCustomerByEmail(authorizationHeader);
        }

        public Employee TryGetEmployeeByEmail(string authorizationHeader)
        {
            return employeeService.GetEmployeeByEmail(authorizationHeader);
        }
    }
}
